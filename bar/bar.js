var addButton = document.querySelector('#template-add-clone .add-save');
var neverButton = document.querySelector('#template-add-clone .never-save');
var closeButton = document.getElementById('close-button')

addButton.addEventListener('click', (e) => {
    e.preventDefault();

    sendPlatformMessage({
        from: 'page',
        subject: 'bgAddSave',
        command: 'bgAddSave'
    });
});

neverButton.addEventListener('click', (e) => {
    e.preventDefault();

    sendPlatformMessage({
        from: 'page',
        subject: 'bgNeverSave',
        command: 'bgNeverSave'
    });
});

closeButton.addEventListener('click', (e) => {
    e.preventDefault();
    sendPlatformMessage({
        from: 'page',
        subject: 'bgClose',
        command: 'bgClose'
    });
});


 


function getQueryVariable(variable) {
    query = window.location.search.substring(1);
    var vars = query.split('&');

    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split('=');
        if (pair[0] === variable) {
            return pair[1];
        }
    }

    return null;
}

function setContent(element) {
    const content = document.getElementById('content');
    while (content.firstChild) {
        content.removeChild(content.firstChild);
    }

    var newElement = element.cloneNode(true);
    newElement.id = newElement.id + '-clone';
    content.appendChild(newElement);
}

function sendPlatformMessage(msg) {
    chrome.runtime.sendMessage(msg);
}
