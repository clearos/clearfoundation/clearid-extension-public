var loginKeywords = ["sign in", "login"];
var changeKeywords = [/[\s\S]*(change)?[\s\w]*password/i];
var accountKeywords = ["account", "settings", "my account", "profile"];
var signoutKeywords = ["sign out", "logout"];

function getUrl(url, callback) {
    $.ajax({
        url: url,
        type: 'get',
        contentType: 'text/html',
        async: true,
        crossDomain: 'true',
        success: function(data, status) {            
            callback(data, null);
        },
        error: function(xhr, status, err) {
            if (xhr.responseText != "") {
                errors =  xhr.responseText;
                callback(null, errors);
            }
        }
    });
}

function redirectClick(status, from, callback) {
    chrome.runtime.sendMessage({from: from, msg: "setStatus", status: status}, function(response) {
        console.log("Redirect status store response was ", response);
        callback();
        if (!response.success && from=="autochanger") {
            chrome.runtime.sendMessage({
                from: from, 
                msg: "failure", 
                url: status.url, 
                key: status.key,
                metadata: {
                    func: "redirectClick.setStatus",
                    status: status.status
                }
            });
        }
    });
}

function redirectGeneric(tag, status, from, newStatus) {
    status.status = newStatus;
    
    if (tag.href != "" && tag.href.indexOf("javascript:") == -1) {
        // Several sites use special subdomains for authentication. When these sites redirect to new URLs
        // they clobber the query string. The only way to maintain state between such requests is to
        // post the status back to the browser extension.
        redirectClick(status, from, function() {
            window.location = tag.href;
        });
    } else {
        // Since we are only doing a javascript event in the existing page, we don't need to go through 
        // the pages onload event, we can call the transition directly. However, we still need to 
        // update the extension with our status.
        redirectClick(status, from, function() {
            tag.click()
            // Since the page is not reloading, we aren't going to get onload to trigger
            // the handler; call it manually.
            if (from == "autochanger") {
                handleTransition(status);
            } else if (from == "navigator") {
                handleNavigation(status);
            }
        });
    }
}

function redirectLogin(tag, status, from) {
    redirectGeneric(tag, status, from, "started");
}

function findGenericLinks(keywords) {
    var found = false;
    var possible = [];
    var links = document.getElementsByTagName('a');
    $.each(links, function(k, tag){
        found = false;
        $.each(keywords, function(i, kw){
            if (!found) {
                if (typeof(kw) == "object") {
                    // This is a regex match pattern
                    if (tag.innerHTML.match(kw) != null) {
                        possible.push(tag);
                        found = true;
                    }
                } else {
                    if (tag.innerHTML.toLowerCase().indexOf(kw) > -1) {
                        possible.push(tag);
                        found = true;
                    }
                };
            }
        });
    });
    console.log("Generic find", links, possible, keywords);

    // If the URL the links point to is the same, we can work off the first candidate.
    var urls = [];
    var uniqueTags = [];
    $.each(possible, function(k, tag){
        var baseUrl = tag.href.split('?')[0];
        if (!urls.includes(baseUrl)) {
            urls.push(baseUrl);
            uniqueTags.push(tag);
        }
    });
    return uniqueTags;
}

function loginWithStatus(from, status) {
    var logoutTags = findGenericLinks(signoutKeywords);
    if (logoutTags.length > 0) {
        // We are already logged in, no need to do the login routine. Send the message to extension
        // to let it know we are logged in, then try and find the password change page.
        status.status = "logged-in";
        chrome.runtime.sendMessage({from: from, msg: "setStatus", status: status}, function(sResponse) {
            console.log("Redirect status store response was ", sResponse);
            // Only the autochanger has more tasks to do from this point forward.
            if (from == "autochanger") {
                if (sResponse.success) {
                    findPasswordChangePage(status, false);
                } else {
                    chrome.runtime.sendMessage({
                        from: from, 
                        msg: "failure", 
                        url: status.url, 
                        key: status.key,
                        metadata: {
                            func: "doLogin.setStatus",
                            status: status.status
                        }
                    });
                }
            }
        });        
    } else {
        analyzeDOM(null, null, ["password", "text|email"], function(loginForm) {
            if (loginForm != null) {
                status.status = "started";
                handleNavigation(status);
            } else {
                findLoginPage(status, from);
            }
        });        
    }
}

/**
 * Attempts a login only if the user isn't already logged in.
 */
function doLogin() {
    chrome.runtime.sendMessage({from: "autochanger", msg: "getStatus", hostname: location.hostname}, function(response) {
        var status = null;
        if (response.status != null) {
            status = JSON.parse(response.status);
        }
        loginWithStatus("autochanger", status);
    });
}

var parser = new DOMParser();
const trueValueLambda = (currentValue) => currentValue;
function analyzeDOM(tag, text, requirements, executor) {
    var html = null;
    if (text == null) {
        html = document;
    } else {
        html = parser.parseFromString(text, "text/html");
    }
    var forms = html.getElementsByTagName('form');
    var iform = 0;
    var form = null;
    let found = false;
    var result = null;

    while (!found && iform < forms.length) {
        form = forms[iform];

        //Make sure the form has a user/email and password type input.
        var hasFlags = {};
        for (var iflag=0; iflag < requirements.length; iflag++) {
            hasFlags[requirements[iflag]] = false;
        }

        var iel = 0;
        while (!Object.values(hasFlags).every(trueValueLambda) && iel < form.elements.length) {
            var el = form.elements[iel];
            var usedFlags = [];

            for (var iflag=0; iflag < requirements.length; iflag++) {
                if (!usedFlags.includes(iflag)) {
                    var parts = requirements[iflag].split('|');
                    if (parts.length > 1) {
                        var orFlag = true;
                        $.each(parts, function(k, p) {
                            orFlag = orFlag || el.type == p;
                        })
                        hasFlags[requirements[iflag]] = orFlag;
                        usedFlags.push(iflag);
                    } else {
                        hasFlags[requirements[iflag]] = el.type == parts[0];
                        usedFlags.push(iflag);
                    }
                }
            }
            iel += 1;
        }

        if (Object.values(hasFlags).every(trueValueLambda)) {
            found = true;
            if (tag != null) {
                result = tag;
            } else {
                result = form;
            }
        }
        iform += 1;
    }

    executor(result);
}

function findForm(status, keywords, requirements, kind, redirector, from="autochanger", notFoundCallback=null) {
    var uniqueTags = findGenericLinks(keywords); 

    if (uniqueTags.length >= 1) {
        console.log("Redirecting to single " + kind + " page at ", uniqueTags[0].href);
        redirector(uniqueTags[0], status, from);
        return true;
    } else if (uniqueTags.length > 1 && requirements.length > 0) {
        var itag = 0;
        var fired = false;
        var xcount = 0;
        var executor = function(tag) {
            xcount += 1;
            if (tag != null) {
                console.log("Redirecting to best " + kind + "page at ", tag.href);
                fired = true;
                redirector(tag, status, from);
            }
            
            if (!fired && xcount == uniqueTags.length && notFoundCallback == null) {
                chrome.runtime.sendMessage({
                    from: "autochanger", 
                    msg: "failure", 
                    url: status.url, 
                    key: status.key,
                    metadata: {
                        func: "find" + kind + "Page.multiTag",
                        status: status.status
                    }
                });
            } else {
                notFoundCallback();
            }
        };

        while (!fired && itag < uniqueTags.length) {
            var tag = uniqueTags[itag];
            getUrl(tag.href, function(text) {
                // Search the contents of the page as this link to see if it has the HTML form
                // we are looking for.
                analyzeDOM(tag, text, requirements, executor);
            });
        }    
    }
    return false;    
}

/**
 * Searches the page text to try and find the login button/link so that we can auto-login
 * the user.
 */
function findLoginPage(status, from) { 
    findForm(status, loginKeywords, ["password", "text|email"], "Login", redirectLogin, from);
}

function redirectAccount(tag, status) {
    redirectGeneric(tag, status, "account");
}

function redirectChange(tag, status) {
    redirectGeneric(tag, status, "changing");
}

function findPasswordChangePage(status, fromAccount) {
    console.log("Looking for account/change password page.");
    // We setup advanced form searches only if we have already tried the find password page.
    var requirements = [];
    if (fromAccount) {
        requirements = ["password", "password", "password"];
    }

    if (!findForm(status, changeKeywords, [], "ChangePassword", redirectChange)) {
        // Let's look for an account page, then re-look for the change password link.
        if (!fromAccount) {
            if (!findForm(status, accountKeywords, [], "Account", redirectAccount)) {
                findPasswordChangePage(status, true);
            }
        }
    }   
}

function findButton(form, tagType) {
    var button = form.querySelector(tagType + '[type=submit]');
    var target = null;
    if (button == null) {
        button = form.querySelector(tagType + tagType == 'input' ? '[type=button]' : '');
        $.each(button, function(k, tag){
            if (tag.innerText.match(changeKeywords[0])) {
                target = tag;
            }
        });
    } else {
        target = button;
    }

    return target;
}

function changePass(status, oldPassword, newPassword) {
    // Find the password fields in the document
    var passwordInputs = document.querySelectorAll("input[type=password]");
    // The current password probably has the word "current" in the name.
    var current = [];
    var newpw = [];
    if (passwordInputs.length != 3) {        
        //Post an error message back to the extension for this URL.
        console.log("Weird number of password fields...", passwordInputs);
        return;
    }

    for (var i = 0; i < passwordInputs.length; i++) {
        if (passwordInputs[i].name.toLowerCase().indexOf("current") > -1) {
            current.push(passwordInputs[i]);
        } else {
            newpw.push(passwordInputs[i]);
        }
    }

    console.log("Found password inputs ", current, newpw);
    if (current.length == 1 && newpw.length == 2) {
        $(current[0]).focus().val(oldPassword).trigger("change").blur();
        $(newpw[0]).focus().val(newPassword).trigger("change").blur();
        $(newpw[1]).focus().val(newPassword).trigger("change").blur();

        // Find the submit button and change the password.
        var form = current[0].form;
        if (form == null) {
            return;
        }

        var target = findButton(form, "button");
        if (target == null) {
            target = findButton(form, "input");
        }

        status.status = "submitted";
        chrome.runtime.sendMessage({from: "autochanger", msg: "setStatus", status: status}, function(sResponse) {
            console.log("Redirect status store response was ", sResponse);
            if (sResponse.success) {
                if (target != null) {
                    $(target).prop("disabled", false).click();
                } else {
                    form.submit();
                }
            }
        });        
    } else {
        chrome.runtime.sendMessage({
            from: "autochanger", 
            msg: "failure", 
            url: status.url, 
            key: status.key,
            metadata: {
                func: "changePass.findForm",
                status: status.status
            }
        });
    }
}

function checkSuccess(status) {

    chrome.runtime.sendMessage({from: "autochanger", msg: "completed", url: status.url, key: status.key});
}