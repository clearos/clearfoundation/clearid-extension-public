///////////////////////////////////////
/////// Define Constants here /////////

var form_filled_with_clear_id = false;


///////////////////////////////////////

function Form(user, pass) {
  this.user = user;
  this.pass = pass;
}

Form.prototype = {

  fillPass: function(user, pass) {
    if(this.user != undefined) {
      this.user.value = user;
    }

    if(this.pass != undefined) {
      this.pass.value = pass;
      this.copyToClipboard(pass);

      // Without the 0-millisecond setTimeout() call the password input does
      // not always receive focus.
      //
      // The 0-milli setTimeout() is a time-honored way of asking a JS engine
      // "Once all the currently-scheduled functions have finished running,
      // please run me as soon as you can." See
      // https://stackoverflow.com/q/779379 and
      // https://johnresig.com/blog/how-javascript-timers-work/ for further
      // discussion of how that works if that doesn't make sense.
      //
      // That said, it is not clear why that scheduling delay is necessary here
      // since the element should just exist and therefore receive focus. A
      // guess might be that focus is explicitly set somewhere on popup close,
      // so we need to wait to set focus on the password input until that has
      // happened.
      var pass_field = this.pass;
      setTimeout(function() {
        pass_field.focus();
      }, 0);
    }

  },

  copyToClipboard: function(txt) {
    const input = document.createElement('input');
    input.style.position = 'fixed';
    input.style.opacity = 0;
    input.value = txt;
    document.body.appendChild(input);
    input.select();
    document.execCommand('copy');
    document.body.removeChild(input);
  }
}

function fillPass(credential)
{
    let keyId = credential.keyId;
    let derived = getDerivedKeys();
    console.assert(derived.keyId == keyId);
    let deCred = decryptCredential(credential, derived);

    var forms = findForms();
    console.log("Found forms ", forms);

    for(var i = 0; i < forms.length; i++) {
        forms[i].user.value = deCred.user;
        forms[i].pass.value = deCred.password;

        var form = closest(forms[i].user, "form");

        // If this is a multi-stage form, then the elements will be hidden and animated
        // using javascript. See how many stages we have.
        var userHidden = (forms[i].user.getAttribute('hidden') == "true" || 
                          forms[i].user.getAttribute('aria-hidden') == "true");
        var passHidden = (forms[i].pass.getAttribute('hidden') == "true" || 
                          forms[i].pass.getAttribute('aria-hidden') == "true");

        // first find the submit button; not all websites use classic `submit` type on their 
        // buttons. Assume that the button in the form is for submitting it.
        var btnSubmit = form.querySelector('button');
        if (btnSubmit == null) {
          btnSubmit = form.querySelector('input[type=button]');
        }
        if (btnSubmit == null) {
          btnSubmit = form.querySelector('input[type="submit"]');
        }
    
        // Set Flag True that I have filled this form using ClearID        
        form_filled_with_clear_id = true;

        if (btnSubmit) {
          // Perform the first click. If we are still here, it means that we have a multi-stage form.
          btnSubmit.click();
          if (userHidden) {
            btnSubmit.click();
          }
          // If we are still here, then even the username must have been magicked away.
          if (passHidden) {
            btnSubmit.click();
          }
          return true;
        } else {
          // finally submit the form if there is no input button.
          form.submit();
        }
    }


    /*if(this.user != undefined) {
      this.user.value = user;
    }

    if(this.pass != undefined) {
      this.pass.value = pass;
    }*/

}

function findForms() {
  if(document.baseURI.includes("amazon")) {
    return findAmazonEmailForms();
  }

  if(document.baseURI.includes("idmsa.apple.com")) {
    return findAppleDeveloperForms();
  }

  return findPasswordForms();
}

// In my tests, even though we should be able to use these all at once to get all of them
// the browser returns nothing; but if they are tried individually, it works...
var selectors = ["input[type=text]", "input[type=email]", "input:not([type])"]
function findPasswordForms() {

  var forms = [];
  var passInputs = document.querySelectorAll("input[type=password]");

  for(var i = 0; i < passInputs.length; i++) {

    var seenForms = []
    var passInput = passInputs[i];
    var formElement = passInput.form;

    if(formElement && (seenForms.indexOf(formElement) == -1)) {
      for (var j = 0; j < selectors.length; j++) {
        var userInput = formElement.querySelector(selectors[j]);
        if (userInput != undefined) {
          forms.push(new Form(userInput, passInput));
        }
      };     
    }

    seenForms.push(formElement);
  }
  return forms;
}

// Special case for Apple Developer portal loging page.
function findAppleDeveloperForms() {

  var forms = [];
  var userInput = document.querySelector("#account_name_text_field");

  if(userInput) {
    forms.push(new Form(userInput, undefined));
  }

  var passwordInput = document.querySelector("input[type=password]");

  if(passwordInput) {
    forms.push(new Form(undefined, passwordInput));
  }

  return forms;
}

// Special case for amazon market and aws login pages.
function findAmazonEmailForms() {

  var forms = [];
  var emailInput = document.querySelector("input[type=email]");

  if(emailInput) {
    forms.push(new Form(emailInput, undefined));
  }

  var passwordInput = document.querySelector("input[type=password]");

  if(passwordInput) {
    forms.push(new Form(undefined, passwordInput));
  }

  // Special input for AWS root login
  var resolvingInput = document.querySelector("input[id=resolving_input]");

  if(resolvingInput) {
    forms.push(new Form(resolvingInput, undefined));
  }

  // Special input for AWS signin
  var nameInput = document.querySelector("input[name=username]");

  if(nameInput && passwordInput) {
    forms.push(new Form(nameInput, passwordInput));
  }

  return forms;
}
function putClearIDIcon()
{
    // Just make it disable for now
    return false;
    var img = document.createElement("img");
    img.style.right = '50px';
    img.style.top = '200px';
    img.style.position = 'fixed';
    img.style.display = 'inline';
    img.setAttribute("src", "https://cdn.shopify.com/s/files/1/0089/0594/9231/files/clearid-logo.png?1097");
    img.setAttribute("width", "75");
    img.setAttribute("id", "clear_id_logo");
    
    document.body.appendChild(img);
    document.getElementById("clear_id_logo").addEventListener("click", loginWithClearID);
}
function closest(el, selector) {
    var matchesFn;

    // find vendor prefix
    ['matches','webkitMatchesSelector','mozMatchesSelector','msMatchesSelector','oMatchesSelector'].some(function(fn) {
        if (typeof document.body[fn] == 'function') {
            matchesFn = fn;
            return true;
        }
        return false;
    })

    var parent;

    // traverse parents
    while (el) {
        parent = el.parentElement;
        if (parent && parent[matchesFn](selector)) {
            return parent;
        }
        el = parent;
    }

    return null;
}


//chrome.runtime.sendMessage("test");

setTimeout(function(){
    if(findForms().length > 0) {
        putClearIDIcon();
    }
},1000);

function runDelayed(state) {
  var f = JSON.parse(localStorage.getItem('clearid/delayed'));
  localStorage.removeItem('clearid/delayed');
  switch (f.func) {
    case "doLoginDelayed":
      doLoginDelayed(f.status, state);
    case "findPasswordChangePage":
      findPasswordChangePage(f.status, f.isAccount);
      break;
    case "changePass":
      changePass(f.status, state.old, state.new);
      break;
    case "checkSuccess":
      checkSuccess(f.status);
      break;
  }
}
function setDelayed(dFunc) {
  localStorage.setItem('clearid/delayed', JSON.stringify(dFunc));
}

function doLoginDelayed(status, state) {
  console.log("Recevied credential for", state.user);

  // There is a good chance that filling the password is going to cause a redirect.
  // Set the status to logged-in so that we can respond correctly when this handler
  // fires on page load again.
  status.status = "logged-in";
  chrome.runtime.sendMessage({from: "autochanger", msg: "setStatus", status: status}, function(statusResponse) {
    if (statusResponse.success) {
      fillPass(state.user, state.password);
    }
  });         
}

function handleNavigation(status) {
  switch (status.status) {
    case "redirect": 
        console.log("Running navigation to login `redirect`.");
        var starter = function() {
          chrome.runtime.sendMessage({from: "navigator", subject: "setStatus", status: status}, function(response) {
            if (response.succes) {
              loginWithStatus("navigator", status);
            }
          });
        }
        if (typeof(doLogin) == "undefined") {
          chrome.runtime.sendMessage({from: "navigator", subject: "redirect", key: status.key, url: status.url}, function(rResponse){
            if (rResponse.success) {
              loginWithStatus("navigator", status);
            }
          });
        } else {
          starter();
        }        
        break;
    case "started":
        console.log("Running navigation to login `started`.");
        status.status = "logged-in";
        chrome.runtime.sendMessage({from: "navigator", subject: "setStatus", status: status}, function(sResponse) {
          if (sResponse.success) {
            chrome.runtime.sendMessage({from: "navigator", subject: "login", url: status.url, key: status.key});
          }
        });
        break;
    case "logged-in":
        console.log("Running navigation `logged-in`.");
        chrome.runtime.sendMessage({from: "navigator", subject: "completed", status: status});
        break;
  }
}

function handleTransition(status) {
  var dFunc = null;
  switch (status.status) {
    case "started":
        console.log("Running password change `started`.");
        chrome.runtime.sendMessage({from: "autochanger", msg: "login", url: status.url, key: status.key}, function(credResponse) {
            if (credResponse.from == "autochanger" && credResponse.success) {
              dFunc = {
                func: "doLoginDelayed",
                status: status
              }
              setDelayed(dFunc);
            }
        });
        break;
    case "logged-in":
        console.log("Running password change `logged-in`.");
        chrome.runtime.sendMessage({from: "autochanger", msg: "inject-pwjs"}, function(logResponse) {
          console.log("Received inject response after logged-in", logResponse);
          if (logResponse.from == "autochanger" && logResponse.success) {
            dFunc = {
              func: "findPasswordChangePage",
              isAccount: false,
              status: status
            }
            setDelayed(dFunc);
          }
        });
        break;
    case "account":
        console.log("Running password change `account`.");
        chrome.runtime.sendMessage({from: "autochanger", msg: "inject-pwjs"}, function(accResponse) {
          if (accResponse.from == "autochanger" && accResponse.success) {
            dFunc = {
              func: "findPasswordChangePage",
              isAccount: true,
              status: status
            }
            setDelayed(dFunc);
          }
        });
        break;
    case "changing":
        console.log("Running password change `changing`.");
        chrome.runtime.sendMessage({from: "autochanger", msg: "change", url: status.url, key: status.key}, function(chResponse) {
            if (chResponse.from == "autochanger" && chResponse.success) {
              dFunc = {
                func: "changePass",
                status: status
              }
              setDelayed(dFunc);
            }
        });
        break;
    case "submitted":
        console.log("Running password change `submitted`.");
        chrome.runtime.sendMessage({from: "autochanger", msg: "inject-pwjs"}, function(subResponse) {
          console.log("Received inject response after logged-in", subResponse);
          // Check whether the password change was successful. Then message the extension.
          dFunc = {
            func: "checkSuccess",
            status: status
          }
          setDelayed(dFunc);
        });
        break;
  }
}

function findFormButtonsOnLoad() {
  var forms = document.forms;

  for (index = 0; index < forms.length; ++index) {
      
      var form = forms[index];

      var submit_founded = false;
      ///// first find the submit button
      var btnSubmit = form.querySelector('button[type="submit"]');
          
      if (btnSubmit) {
          btnSubmit.addEventListener("click", submitted);
          submit_founded = true;
      }

      ///// Now check for input submit button
      var inputSubmit = form.querySelector('input[type="submit"]');
      
      if (inputSubmit) {
          inputSubmit.addEventListener("click", submitted);
          submit_founded = true;
      }

      // if there is not submit button check for button only
      if (!submit_founded) {
          var btn = form.querySelector('button');

          if (btn) {
              btn.addEventListener("click", submitted);
          }

      }

      //// form submit handler
      if (form.attachEvent) {
          form.attachEvent("submit", submitted);
      } else {
          form.addEventListener("submit", submitted);
      }
  }

  //form.onsubmit = submitted.bind(form);
}

/**
 * Queries the extension for this website's specific derived keys, if they aren't already available
 * in local storage.
 */
function getDerivedKeys() {
  let ds = localStorage.getItem("clearid/derived");
  var derived = null;
  if (ds != null) {
    derived = JSON.parse(ds);
    derived.keyPair.publicKey = new Uint8Array(Object.values(derived.keyPair.publicKey));
    derived.keyPair.privateKey = new Uint8Array(Object.values(derived.keyPair.privateKey));
  }
  return derived;
}

// Listener when form is submitted
window.onload = function() {
  setTimeout(function() {
    var dKeys = localStorage.getItem("clearid/derived");
    var haveKeys = (dKeys != null);
    chrome.runtime.sendMessage({from: "crypto", msg: "load", hostname: location.hostname, haveKeys: haveKeys});
  }, 500);

  setTimeout(function() {
    // First, check local storage for the autochange status. If it isn't there, we must have changed
    // domains during the redirect for authentication. In that case, check the query string.
    chrome.runtime.sendMessage({from: "navigator", subject: "getStatus", hostname: location.hostname}, function(nResponse) {
      if (nResponse.success && nResponse.status != null && nResponse.status != "null") {
        handleNavigation(JSON.parse(nResponse.status));
      } else {
        chrome.runtime.sendMessage({from: "autochanger", msg: "getStatus", hostname: location.hostname}, function(response) {
          var autochangeStatus = null;
          if (response != undefined && response.from == "autochanger" && response.success) {           
            autochangeStatus = response.status;
            console.log("AutoChange status", autochangeStatus);
          }
    
          if (autochangeStatus != null) {
            handleTransition(JSON.parse(autochangeStatus));
          } else {    
            findFormButtonsOnLoad();
            getDerivedKeys();
          }
        });    
      }      
    });
    
    
  }, 1500);
}

function submitted(event) {

    // if form is already filled with ClearID then don't show save bar
    if(form_filled_with_clear_id)
        return false;

    // Update the relevant fields with the new data.
    const thisIsCallback = info => {

        if (!info.error) {
            push_iframe();
        }
    };
    
   /* chrome.runtime.sendMessage({
                from: 'content',
                subject: 'formSubmitClicked',
                host: "host",
                user: "user",
                pass: "pass",
            }, thisIsCallback);*/

    var formsObj = findForms();
    console.log(formsObj);
    
    if(formsObj.length > 0) {
        
        for(var i = 0; i < formsObj.length; i++) {

            var user = formsObj[i].user.value;
            var pass = formsObj[i].pass.value;

            var host = window.location.hostname;

            if (user && pass && host) {
                chrome.runtime.sendMessage({
                    from: 'content',
                    subject: 'formSubmitClicked',
                    host: host,
                    user: user,
                    pass: pass
                }, thisIsCallback);
            }
        }
    }
    //event.preventDefault();
}
function loginWithClearID($this)
{
    var host = window.location.hostname;

    chrome.runtime.sendMessage({
        from: 'content',
        subject: 'showQRCode',
        host: host,
    });
}

function push_iframe()
{
    closeBar();

    var barPage = "bar/save_pop.html";

    var barPageUrl = chrome.extension.getURL(barPage);

    const iframe = document.createElement('iframe');
    iframe.style.cssText = 'height: 42px; width: 100%; border: 0; min-height: initial;';
        iframe.id = 'clear_id_notification_iframe';

        const frameDiv = document.createElement('div');
        frameDiv.setAttribute('aria-live', 'polite');
        frameDiv.id = 'clear_id_notification_bar';
        frameDiv.style.cssText = 'height: 42px; width: 100%; top: 0; left: 0; padding: 0; position: fixed; ' +
            'z-index: 2147483647; visibility: visible;';
        frameDiv.appendChild(iframe);
        document.body.appendChild(frameDiv);

        iframe.contentWindow.location  = barPageUrl;

        const spacer = document.createElement('div');
        spacer.id = 'clear_id_notification_bar_spacer';
        spacer.style.cssText = 'height: 42px;';
        document.body.insertBefore(spacer, document.body.firstChild);
}

///////////////////////////////////////////////
/// Start When input focused 
//////////////////////////////////////////////

function push_suggestion_iframe(eleObj)
{
    closeBar();

    var offsetLeft = eleObj.target.offsetLeft;
    var offsetTop = eleObj.target.offsetTop - (window.pageYOffset || (document.documentElement || document.body.parentNode || document.body).scrollTop);
    var offsetHeight = eleObj.target.offsetHeight;
    var margin = 0;

    var total_top_margin = parseInt(offsetTop) + parseInt(offsetHeight) + parseInt(margin);

    console.log(offsetLeft);

    /////////////////

    var injectPage = "inject/suggestions.html";

    var injectPageUrl = chrome.extension.getURL(injectPage);

    const iframe = document.createElement('iframe');
    iframe.style.cssText = 'height: auto; width: auto; border: 0; min-height: initial;';
        iframe.id = 'clear_id_inject_iframe';
        iframe.class = 'ready';

        var link = document.createElement("link");
        link.href = chrome.extension.getURL("inject/inject.css");
        link.type = "text/css";
        link.rel = "stylesheet";


        const frameDiv = document.createElement('div');
        frameDiv.setAttribute('aria-live', 'polite');
        frameDiv.id = 'clear_id_notification_bar';
        frameDiv.style.cssText = 'height: auto; width: 100%; left: 0; padding: 0; position: fixed; ' +
            'z-index: 2147483647; visibility: visible;';
        frameDiv.style.top = total_top_margin + 'px';
        frameDiv.style.left = offsetLeft + 'px';
        frameDiv.appendChild(link);
        frameDiv.appendChild(iframe);
        document.body.appendChild(frameDiv);

        iframe.contentWindow.location  = injectPageUrl;

        const spacer = document.createElement('div');
        spacer.id = 'clear_id_notification_bar_spacer';
        spacer.style.cssText = 'height: 0px;';
        document.body.insertBefore(spacer, document.body.firstChild);
}
function activeFocusIn(e) 
{
    if (e.target.tagName == "INPUT" || e.target.tagName == "input") {
        

        push_suggestion_iframe(e);
    }
}

//document.addEventListener('focus', activeFocusIn, true);

/////////////////////////////////////////////////
//////// End input focus
////////////////////////////////////////////////
function closeBar() 
{
        const barEl = document.getElementById('clear_id_notification_bar');
        if (barEl != null) {
            barEl.parentElement.removeChild(barEl);
        }

        const spacerEl = document.getElementById('clear_id_notification_bar_spacer');
        if (spacerEl) {
            spacerEl.parentElement.removeChild(spacerEl);
        }
}

// Inform the background page that 
// this tab should have a page-action.

chrome.runtime.sendMessage({
  from: 'content',
  subject: 'showPageAction',
});

// Listen for messages from the popup.
chrome.runtime.onMessage.addListener((msg, sender, response) => {
  console.log("Received message", msg);
    // First, validate the message's structure.
    if ((msg.from === 'popup') && (msg.subject === 'DOMInfo')) {
        // Collect the necessary data. 
        // (For your specific requirements `document.querySelectorAll(...)`
        //  should be equivalent to jquery's `$(...)`.)
        var domInfo = {
          total: document.querySelectorAll('total').length,
          inputs: document.querySelectorAll('input').length,
          buttons: document.querySelectorAll('button').length,
        };

        // Directly respond to the sender (popup), 
        // through the specified callback.
        response(domInfo);

    } else if ((msg.from === 'popup') && (msg.subject === 'TabInfo')) {
        
        // Collect the necessary data. 
        //alert(window.location.hostname);
        var tabInfo = {
            hostname: window.location.hostname,
        };

        // Directly respond to the sender (popup), through the specified callback.
        response(tabInfo);

    } else if ((msg.from === 'popup') && (msg.subject === 'FillPassword')) {
        
        // Collect the necessary data. 
        //alert(window.location.hostname);
        //alert(msg.user);
        findForms();
        setTimeout(function(){
            fillPass(msg.credential);

        },200);
        var fillPassword = {
            success: true,
        };

        // Directly respond to the sender (popup), through the specified callback.
        response(fillPassword);
  
    } else if (msg.from == 'navigator' && msg.subject == 'navigateLogin') {
      console.log("Received login message from extension.");
      var status = {
        status: "pending",
        action: "navigate",
        key: msg.key,
        url: msg.url
      }  
      chrome.runtime.sendMessage({from: "navigator", subject: "setStatus", status: status}, function(response) {
        if (response.succes) {
          doLogin("navigator");
        }
      });
    } else if ((msg.from === 'background') && (msg.subject === 'addSaveBar')) {
        alert("sdf");

    } else if ((msg.from == 'background') && (msg.subject == 'closeSaveBar')) {
        closeBar();
    
    } else if ((msg.from == 'background') && (msg.subject == 'showSaveBar')) {
        //push_iframe();
    } else if (msg.from == 'autochanger' && msg.subject == 'login') {
      console.log("Received login message from extension.");
      var status = {
        "status": "pending",
        "url": msg.url,
        "key": msg.key
      }  
      response({success: true, status: status});
      setTimeout(function() { 
        doLogin();
      }, 500);            
    } else if (msg.from == 'autochanger' && msg.subject == 'delayed') {
      setTimeout(function() {
        runDelayed(msg.state);
      }, 50);
      response({success: true});
    } else if (msg.from == "crypto" && msg.subject == "derived") {
      console.log("Received crypto:derived response", msg);
      if (msg.success) {
        localStorage.setItem("clearid/derived", JSON.stringify(msg.keys));
        response({success: true});
      }
    }
});

