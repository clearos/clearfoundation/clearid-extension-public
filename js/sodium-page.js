var api_url = "https://decentralized.clearfoundation.com/fido/";

/** Since Base58 is so small, to reduce injection operations I added it here. */
(function() {
    var ALPHABET, ALPHABET_MAP, Base58, i;
  
    Base58 = (typeof module !== "undefined" && module !== null ? module.exports : void 0) || (window.Base58 = {});
  
    ALPHABET = "123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz";
  
    ALPHABET_MAP = {};
  
    i = 0;
  
    while (i < ALPHABET.length) {
      ALPHABET_MAP[ALPHABET.charAt(i)] = i;
      i++;
    }
  
    Base58.encode = function(buffer) {
      var carry, digits, j;
      if (buffer.length === 0) {
        return "";
      }
      i = void 0;
      j = void 0;
      digits = [0];
      i = 0;
      while (i < buffer.length) {
        j = 0;
        while (j < digits.length) {
          digits[j] <<= 8;
          j++;
        }
        digits[0] += buffer[i];
        carry = 0;
        j = 0;
        while (j < digits.length) {
          digits[j] += carry;
          carry = (digits[j] / 58) | 0;
          digits[j] %= 58;
          ++j;
        }
        while (carry) {
          digits.push(carry % 58);
          carry = (carry / 58) | 0;
        }
        i++;
      }
      i = 0;
      while (buffer[i] === 0 && i < buffer.length - 1) {
        digits.push(0);
        i++;
      }
      return digits.reverse().map(function(digit) {
        return ALPHABET[digit];
      }).join("");
    };
  
    Base58.decode = function(string) {
      var bytes, c, carry, j;
      if (string.length === 0) {
        return new (typeof Uint8Array !== "undefined" && Uint8Array !== null ? Uint8Array : Buffer)(0);
      }
      i = void 0;
      j = void 0;
      bytes = [0];
      i = 0;
      while (i < string.length) {
        c = string[i];
        if (!(c in ALPHABET_MAP)) {
          throw "Base58.decode received unacceptable input. Character '" + c + "' is not in the Base58 alphabet.";
        }
        j = 0;
        while (j < bytes.length) {
          bytes[j] *= 58;
          j++;
        }
        bytes[0] += ALPHABET_MAP[c];
        carry = 0;
        j = 0;
        while (j < bytes.length) {
          bytes[j] += carry;
          carry = bytes[j] >> 8;
          bytes[j] &= 0xff;
          ++j;
        }
        while (carry) {
          bytes.push(carry & 0xff);
          carry >>= 8;
        }
        i++;
      }
      i = 0;
      while (string[i] === "1" && i < string.length - 1) {
        bytes.push(0);
        i++;
      }
      return new (typeof Uint8Array !== "undefined" && Uint8Array !== null ? Uint8Array : Buffer)(bytes.reverse());
    };
  
  }).call(this);

function getBaseDomain(url) {
    var hostname = url;
    if (url.indexOf('//') > -1) {
        hostname = url.split('//')[1];
        if (hostname.indexOf('/') > -1) {
            hostname = hostname.split('/')[0];
        }
    }

    // We are interested in only the last 2 parts of the hostname (subdomains removed).
    var parts = hostname.split('.');
    return parts[parts.length-2] + '.' + parts[parts.length-1];
}

function findKeyId(baseUrl) {
    let keyId = localStorage.getItem("clearid/" + baseUrl + "/keyId");
    if (keyId == null) {
        keyId = 0;
        localStorage.setItem("clearid/" + baseUrl + "/keyId", keyId);
    } else {
        keyId = Number.parseInt(keyId);
    }
    return keyId;
}

function getCurrentAppKey(hostname) {
    let baseUrl = getBaseDomain(hostname);
    let keyId = findKeyId(baseUrl);
    return deriveAppKey(baseUrl, keyId);
}

/**
 * Creates a new Key pair and extracts the DID and verkey from it.
 * @param {String} seed The seed string to use when creating the keypair.
 */
function createDid(seed) {
    let keyPair = createKeyPair(seed);
    return {
        "did": Base58.encode(keyPair.publicKey.slice(0, 16)),
        "verkey": Base58.encode(keyPair.publicKey)
    }
};

/**
 * Creates a new cryptographic keypair of public and private key.
 * @param {String} seed Seed string to use when creating the keypair.
 */
function createKeyPair(seed) {
    if (typeof(seed) == "string") {
        return sodium.crypto_sign_seed_keypair(sodium.from_string(seed));
    } else {
        return sodium.crypto_sign_seed_keypair(seed);
    }
};

/**
 * Generates a random cryptographic key pair.
 */
function randomKeyPair() {
    seed = sodium.randombytes_buf(sodium.crypto_box_SEEDBYTES);
    kp = sodium.crypto_sign_seed_keypair(seed);
    return {
        "keyPair": kp,
        "verkey": Base58.encode(kp.publicKey)
    }
}

function genericHash(value) {
    let hashBytes = sodium.crypto_generichash(64, sodium.from_string(value));
    return sodium.to_base64(hashBytes);
}

function deriveAppKey(baseUrl, keyId) {
    let kp = getVaultKeys();
    let len = sodium.crypto_secretbox_KEYBYTES;
    let context = genericHash(baseUrl).slice(0, 8);
    let seed = sodium.crypto_kdf_derive_from_key(len, keyId, context, kp.privateKey.slice(0, len));
    kp = sodium.crypto_sign_seed_keypair(seed);
    return {
        "keyPair": kp,
        "verkey": Base58.encode(kp.publicKey),
        "keyId": keyId
    }
}

/**
 * Encrypts a plain-text password using either vault keys or a specific derived key.
 * @param {Object} credential Plain-text user and password in an object.
 * @param {*} derived Derived keypair to use for the encryption. Vault keys are used if not given.
 */
function encryptCredential(credential, derived) {
    var kp = null;
    var result = {
        user: null,
        password: null
    }

    if (derived == null) {
        kp = getVaultKeys();
        result.user = encryptSingle(credential.user, kp);
        result.password = encryptSingle(credential.password, kp);
    } else {
        kp = derived;
        result.keyId = derived.keyId;
        result.user = encryptSingle(credential.user, kp.keyPair);
        result.password = encryptSingle(credential.password, kp.keyPair);
    }
    return result
}

/**
 * Encrypts a string for a particular public key.
 * @param {String} value plain-text value to encrypt.
 * @param {Object} kp Keypair that will be able to decrypt the value; only public key is used.
 */
function encryptSingle(value, kp) {
    let encval = anonCrypt(sodium.from_string(value), kp.publicKey);
    return btoa(fromUint8(encval));
}

/**
 * Decrypts a value that was encrypted using `encryptSingle`.
 * @param {String} value Encrypted value returned by `encryptSingle`.
 * @param {Object} kp Keypair to use for the decryption.
 */
function decryptSingle(value, kp) {
    return sodium.to_string(anonDecrypt(toUint8(atob(value)), kp.publicKey, kp.privateKey));
}

/**
 * Decrypts the given credential using the vault keys by default, or a derived keypair if specified.
 * @param {Object} credential Object with keys `user` and `password` that are encrypted.
 */
function decryptCredential(credential, derived) {
    var kp = null;
    var result = {
        user: null,
        password: null
    }
    if (derived == null) {
        kp = getVaultKeys();
        result.user = decryptSingle(credential.user, kp);
        result.password = decryptSingle(credential.password, kp);
    } else {
        kp = derived;
        result.user = decryptSingle(credential.user, kp.keyPair);
        result.password = decryptSingle(credential.password, kp.keyPair);
    }

    return result
}

/**
 * Encrypts a message anonymously.
 * @param {String} msg The message to encrypt.
 * @param {Uint8Array} to_pk The public key whose private key will decrypt the message.
 */
function anonCrypt(msg, to_pk) {
    pk = sodium.crypto_sign_ed25519_pk_to_curve25519(to_pk);
    if (typeof(msg) == "string") {
        return sodium.crypto_box_seal(sodium.from_string(msg), pk);
    } else {
        return sodium.crypto_box_seal(msg, pk);
    }
}

/**
 * Decrypts a message that was encrypted using anonCrypt.
 * @param {String} enc Encrypted string to decrypt, should be Base64 encoded.
 * @param {Uint8Array} pk Public key that matches the one the message was encrypted for.
 * @param {Uint8Array} sk Corresponding private key.
 */
function anonDecrypt(enc, pk, sk) {
    _pk = sodium.crypto_sign_ed25519_pk_to_curve25519(pk);
    _sk = sodium.crypto_sign_ed25519_sk_to_curve25519(sk);
    if (typeof(msg) == "string") {
        msg = sodium.crypto_box_seal_open(sodium.from_string(enc), _pk, _sk);
    } else {
        msg = sodium.crypto_box_seal_open(enc, _pk, _sk);
    }
    
    return msg;
}

/**
 * Generates the vault encryption keypair.
 */
function getVaultKeys() {
    let password = sodium.from_base64(localStorage.getItem('password'));
    let localpk = createKeyPair(password);
    let vaultSeed = sodium.from_base64(localStorage.getItem('vault-seed'));
    let decSeed = anonDecrypt(vaultSeed, localpk.publicKey, localpk.privateKey);
    return createKeyPair(decSeed);
}

/**
 * Securely encrypts the vault seed needed to generate the vault keys.
 * @param {String} password The decrypted password that should be used to secure the vault keys.
 * @param {Sring} vaultSeed The decrypted vault seed that was sent by the Android App.
 */
function setVaultSeed(password, vaultSeed) {
    localStorage.setItem('password', sodium.to_base64(password));
    let localpk = createKeyPair(password);
    let encSeed = anonCrypt(vaultSeed, localpk.publicKey);
    localStorage.setItem('vault-seed', sodium.to_base64(encSeed));
}

/**
 * Encrypts the contents of a data object using the saved vault keys.
 * @param {*} obj The object to encrypt values for. The object should have an attribute `fields` whose values will be encrypted.
 */
function vaultCrypt(obj) {
    let kp = getVaultKeys();
    return encryptData(obj, kp.publicKey);
}

/**
 * Decrypts encrypted values in a vault object using the saved vault keys.
 * @param {Object} obj The object with encrypted values. Should have an attribute `fields` with encrypted `value` keys.
 */
function vaultDecrypt(obj) {
    let kp = getVaultKeys();
    return decryptData(obj, kp.publicKey, kp.privateKey);
}

/**
 * Encrypts the contents of a data object for the user vault.
 * @param {*} obj The object to encrypt values for. The object should have an attribute `fields` whose values will be encrypted.
 * @param {*} pk The public key that the values should be encrypted for. Should be the vault key.
 */
function encryptData(obj, pk) {
    for (i in obj["fields"]) {
        let entry = obj["fields"][i];

        // Ignore if fields are from not encrypted data
        if (obj["fields"][i]["name"] == "created" || obj["fields"][i]["name"] == "version")
            continue;

        let encval = anonCrypt(sodium.from_string(entry["value"]), pk);
        obj["fields"][i]["value"] = btoa(fromUint8(encval));
    }
    return obj;
}

/**
 * Decrypts encrypted values in a vault object.
 * @param {Object} obj The object with encrypted values. Should have an attribute `fields` with encrypted `value` keys.
 * @param {Uint8Array} pk Public key that the values were encrypted for.
 * @param {Uint8Array} sk Corresponding secret/private key.
 */
function decryptData(obj, pk, sk) {
    for (i in obj["fields"]) {
        let entry = obj["fields"][i]["value"];

        // Ignore if fields are from not encrypted data
        if (obj["fields"][i]["name"] == "created" || obj["fields"][i]["name"] == "version")
            continue;

        let encval = anonDecrypt(toUint8(atob(entry)), pk, sk);
        obj["fields"][i]["value"] = sodium.to_string(encval);
    }
    return obj;
}

/**
 * Encrypts a message the includes the verkey of the sender. Returns Uint8Array.
 * @param {String} msg The message to encrypt.
 * @param {Uint8Array} to_pk Public key of the recipient of the encrypted message.
 * @param {Uint8Array} from_pk Public key of the sender.
 * @param {Uint8Array} from_sk Secret key of the sender.
 */
function authCrypt(msg, to_pk, from_pk, from_sk) {
    nonce = sodium.randombytes_buf(sodium.crypto_box_NONCEBYTES);
    _to_pk = sodium.crypto_sign_ed25519_pk_to_curve25519(to_pk);
    _from_sk = sodium.crypto_sign_ed25519_sk_to_curve25519(from_sk);
    enc_body = sodium.crypto_box_easy(msg, nonce, _to_pk, _from_sk);
    combo_box = {
        "msg": sodium.to_base64(enc_body),
        "sender": Base58.encode(from_pk),
        "nonce": sodium.to_base64(nonce)
    };

    combo_box_bin = MessagePack.encode(combo_box);
    enc_message = sodium.crypto_box_seal(combo_box_bin, _to_pk);
    return enc_message
}

/**
 * Decrypts a message encrypted using `authCrypt`.
 * @param {Uint8Array} enc Encrypted message box.
 * @param {Uint8Array} pk Public key of the recipient.
 * @param {Uint8Array} sk Secret key of the recipient.
 */
function authDecrypt(enc, pk, sk) {
    _pk = sodium.crypto_sign_ed25519_pk_to_curve25519(pk);
    _sk = sodium.crypto_sign_ed25519_sk_to_curve25519(sk);
    body = sodium.crypto_box_seal_open(enc, _pk, _sk);

    unpacked = MessagePack.decode(body);
    sender_vk = unpacked.sender;
    nonce = sodium.from_base64(unpacked.nonce);
    enc_message = sodium.from_base64(unpacked.msg);
    sender_pk = sodium.crypto_sign_ed25519_pk_to_curve25519(Base58.decode(sender_vk));
    message = sodium.crypto_box_open_easy(enc_message, nonce, sender_pk, _sk);
    return {
        "message": sodium.to_string(message),
        "sender_vk": sender_vk
    }
}

/**
 * Converts a string into a Uint array of bytes.
 * @param {String} s String to convert to a Uint8 array.
 */
function toUint8(s) {
  let bytes = [];
  for (var i = 0; i < s.length; ++i) {
    var code = s.charCodeAt(i);
    bytes = bytes.concat([code]);
  }
  return Uint8Array.from(bytes);
}

/**
 * Converts an array of Uint8 back to a string.
 */
function fromUint8(a) {
    var encodedString = String.fromCharCode.apply(null, a);
    return encodedString;
}

/**
 * Decrypts a message that was encrypted using the Android App. Because the crypto in the browser runs using
 * Uint8 arrays and the App uses byte arrays, they are not transparently interoperable. Also, the base64 
 * encoding that ships with sodium.js is not consistent with regular Base64 (like `atob` or `btoa` and the 
 * implementations that ship with `python` and `java`, which all match each other).
 * @param {String} msg The encrypted message from the app.
 * @param {*} keyPair The cryptographic key pair whose public key (verKey) the message was encrypted for.
 */
function decryptFromApp(msg, keyPair) {
  let bmsg = toUint8(atob(msg));
  let decrypted = anonDecrypt(bmsg, keyPair.publicKey, keyPair.privateKey);
  let decoded = atob(sodium.to_string(decrypted));
  return toUint8(decoded);
}

/**
 * Begins a pairing transaction between the browser plug-in and an Android App by requesting a QR code
 * based on the public key part of the random keypair used to end-to-end encrypt the vault seed and
 * password.
 * @param {*} keypair The random cryptographic keypair created on the browser for this transaction.
 */
function startPair(keypair) {
    console.log(keypair)
    $.ajax({
        url: api_url + 'passwordless/pair',
        data: {
            verkey: keypair.verkey
        },
        type: 'get',
        dataType: 'json',
        async: true,
        crossDomain: 'true',

        success: function(data, status) {
            console.log(data);
            $("#clear_id_qr_code").attr('src', 'data:image/png;base64,' + data.qrcode);
            $("#clear_id_qr_code").attr('data-challenge', data.challenge);

            getDid(keypair);


        },
        error: function(xhr, status, err) {
            var error = JSON.parse(xhr.responseText);
            show_message('error', "Error!", error.message);
        }
    });
}

/**
 * Requests the DID, vault seed, password and a server API token from the user's home server. This data
 * is mostly created by the mobile app using encryption that only `keypair` can decrypt. The token is 
 * auto-generated by the home server so it can identify the approved pairing relationship in future requests.
 * @param {*} keypair The random cryptographic keypair created on the browser for this transaction.
 */
function getDid(keypair) {
    $.ajax({
        url: api_url + 'passwordless/get-did',
        data: {
            challenge: keypair.verkey,
        },
        type: 'get',
        dataType: 'json',
        async: true,
        crossDomain: 'true',

        success: function(data, status) {
            if (data.did) {
                localStorage.setItem('did', data.did);
                localStorage.setItem('token', data.token);

                // We get the vault key from this payload as well... Decrypt it using our keypair.
                let kp = keypair.keyPair;
                let vaultSeed = decryptFromApp(data.vault_seed, kp);
                let password = decryptFromApp(data.password, kp);

                // Store this vault seed in the keystore.
                setVaultSeed(password, vaultSeed);

                window.location.href = "pair-success.html";
            }
        },
        error: function(xhr, status, err) {
            var error = JSON.parse(xhr.responseText);
            show_message('error', "Server Error!", error.message + " Please do logout and try again");
        }
    });
}

function initQRCode() {
    setTimeout(function() {

        // First, generate a new random keypair and get the verkey.
        let kp = randomKeyPair();
        console.log(kp);
        startPair(kp);
    }, 1000);
};

