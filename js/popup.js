if(!localStorage.getItem('did')) {
    window.location.href = "qr_code.html";
} else {
    $(".clearid-not-pair").hide();
    $(".clearid-paired").show();
}

$(function() {
    $("#unlink_btn").click(function(){
        localStorage.clear();
        window.location.href = "qr_code.html";
    })
});


// Update the relevant fields with the new data.
const setTabInfo = info => {

    // check hostname exists because sometimes it may not
    if (info) {
        var hostname = info.hostname;
        //$(".current_tab_logins").html("<p>Please wait fetching saved logins of <strong>"+info.hostname+"</strong> ..<br> ClearID sent a push notification to your device, please review it</p>");
        $(".current_tab_logins").html("<p>Please wait checking logins of <strong>"+info.hostname+"</strong> ...</p>");
        check_local_credentials(hostname);
        $(".current_website_name").html("<strong>"+hostname+"</strong>");

    
    // Handle if PoPup was opened without TAB URL
    } else {
        $(".current_tab_logins").html('<p class="no-record">No logins available for current tab</p>');
        $(".current_website_saved_logins").text(0);
    }

};

// Once the DOM is ready...
window.addEventListener('DOMContentLoaded', () => {
  // ...query for the active tab...
  chrome.tabs.query({
    active: true,
    currentWindow: true
  }, tabs => {
    // ...and send a request for the DOM info...
    chrome.tabs.sendMessage(
        tabs[0].id,
        {from: 'popup', subject: 'TabInfo'},
        // ...also specifying a callback to be called 
        //    from the receiving end (content script).
        setTabInfo);
  });
});

function check_local_credentials(hostname)
{
    console.log(hostname);
    if (!hostname) {
        return false;
    }
    hostname = remove_sub_domain(hostname);
    //hostname = hostname.replace('www.', '');
    
    var html = "";
    var domains = JSON.parse(localStorage.getItem('credentials'));
    var credentials_length = 0;
    console.log(domains);
    
    $(".current_tab_logins").html("");
    $.each(domains, function(key, domain) {
        if ((hostname) == remove_sub_domain(domain.url)) {

            console.log(domain);

            $.each(domain.entries, function(k, entry) {
        
                credentials_length = credentials_length + 1;
                var html = '<div class="repeat-bx-content-row">'  
                            + '<div class="row align-items-center">' 
                            +     '<div class="col google-icon"><div class="icon"><img src="http://favicon.yandex.net/favicon/'+domain.url+'" alt="icon"></div></div>'
                            +         '<div class="col-6"><div class="account-detail"><h2>'+entry.hint+'</h2><p>'+domain.url+'</p></div></div>' 
                            +         '<div class="col"><div class="actions-btns"><span><a href="http://'+domain.url+'" target="_blank"><img src="../images/share.png"></a></span><span data-user="'+entry.key+'" data-url="'+domain.url+'" class="put_password"><img src="../images/key.png"></span></div></div>'
                            +     '</div>'
                            + '</div>';

                $(".current_website_saved_logins").text(credentials_length);
                $(".current_tab_logins").append(html);
            });
        }
    });

    if (credentials_length == 0) {
        $(".current_tab_logins").html('<p class="no-record">No logins available for '+hostname+'</p>');
        $(".current_website_saved_logins").text(0);
    }
}


// 
var api_url = "https://decentralized.clearfoundation.com/fido/";

function get_credentials(url)
{
    if (!url)
        return false;
    console.log("==>>");   
    console.log(url);   
    console.log(localStorage.getItem('token'));   
    console.log(localStorage.getItem('did'));   
    console.log("==>>");   
    $.ajax({
        url: api_url + 'passwordless/credentials',
        data: { 
            url: url, 
            token: localStorage.getItem('token'), 
            did: localStorage.getItem('did'), 
        },
        type: 'get',
        dataType: 'json',
        async: false,
        crossDomain: 'true',

        success: function(data, status) {
            console.log("success");   
        },
        error: function(xhr) {
            console.log("error");   
            
        }
    });
}