var api_url = "https://decentralized.clearfoundation.com/fido/";

$(document).ready(function(){
    $('#search').focus();

    var domain;
    var password = [];
    
    $(function() {
        //localStorage.clear();
        
        $('#alertMsg').hide();
        if (localStorage.getItem('username') === null) 
            $('#alertMsg').show();
        if (localStorage.getItem('password') === null)
            localStorage.setItem('password', password);
        if (localStorage.getItem('state') === null) 
            localStorage.setItem('state', 'register');
        if (localStorage.getItem('ekey') === null) 
            localStorage.setItem('ekey', '<pP10d%dk;al![2089]$ah!r');
        
        if (localStorage.getItem('username') !== null ) {
            console.log('Username: ' + localStorage.getItem('username') + '\nEncrypted Master: ' + localStorage.getItem('master'));
            console.log('Real Password: ' + GibberishAES.dec(localStorage.getItem('master'), localStorage.getItem('ekey')));
            console.log('SJCL Encrypt: ' + sjcl.encrypt(localStorage.getItem('ekey'), localStorage.getItem('master')));
            console.log('SJCL Decrypt: ' + sjcl.decrypt(localStorage.getItem('ekey'), sjcl.encrypt(localStorage.getItem('ekey'), localStorage.getItem('master'))));
        }
        
        if (localStorage.getItem('state') == 'register') {
            $('#registerPanel').show();
            $('#deletePanel').hide();
            $('#userPanel').hide();
        } else if (localStorage.getItem('state') == 'delete') {
            $('#registerPanel').hide();
            $('#deletePanel').show();
            $('#userPanel').hide();
        } else if (localStorage.getItem('state') == 'login') {
            $('#registerPanel').hide();
            $('#deletePanel').hide();
            $('#userPanel').show();
        }
        
        $('#saveChange').hide();
        
        /*chrome.tabs.query({'active': true, 'lastFocusedWindow': true}, function (tabs) {
            var url = tabs[0].url;
    
            if (url.indexOf("://") > -1) domain = url.split('/')[2];
            else domain = url.split('/')[0];
    
            domain = domain.split(':')[0];
            $('#site').val(domain);
            
            console.log('Domain: ' + localStorage.getItem(domain));
            console.log('ID: ' + localStorage.getItem(domain+'id'));
            console.log('Pass: ' + localStorage.getItem(domain+'pass'));
            console.log('Ekey: ' + localStorage.getItem(domain+'ekey'));
            
            
            if (localStorage.getItem(domain+'id') === null) {
                $('#not').show();
                $('#managed').hide();
            } else {
                $('#not').hide();
                $('#managed').show();
                
                $('#copyUser').val(localStorage.getItem(domain+'id'));
                $('#copyPass').val(localStorage.getItem(domain+'pass'));
            }
        });*/
    });
    
    // :: function
    // :: Password Generator
    // Description: Generates Password According to checked Checkboxes and Specified Length
    $('#generate').click(function() {
        var array = [];
        
        var alphabet = "abcdefghijklmnopqrstuvwxyz".split("");
        for (i=0; i<alphabet.length; ++i) array.push(alphabet[i]);
        
        if ( $('input[name=upper]').is(':checked') ) {
            var alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.split('');
            for (i=0; i<alphabet.length; ++i) array.push(alphabet[i]);
        }
        
        if ( $('input[name=numeric]').is(':checked') ) {
            var number = '0123456789'.split('');
            for (i=0; i<number.length; ++i) array.push(number[i]);
        }
        
        if ( $('input[name=spChar]').is(':checked') ) {
            var special = '!@#$%^&*(){}][:;><,.]'.split('');
            for (i=0; i<special.length; ++i) array.push(special[i]);
        }
        
        var pass = "";
        for (i=0; i<parseInt($('input[name=plength]').val()); ++i) {
            pass += array[Math.floor(Math.random() * array.length)];
        } 
        
        $('#gpwd').val(pass);
    });

});

function copyToClipboard(elementId, domain, val) {  
    var aux = document.createElement("input");
    
    if ( val == 0 ) aux.setAttribute("value", $('#' + elementId.id).val());
    else aux.setAttribute("value", GibberishAES.dec($('#' + elementId.id).val(), localStorage.getItem(domain + 'ekey')));
    
    document.body.appendChild(aux);
    aux.select();
    document.execCommand("copy");

    document.body.removeChild(aux);
}

function pseudoRandom() {
    var array = [];
        
    var alphabet = "abcdefghijklmnopqrstuvwxyz".split("");
    for (i=0; i<alphabet.length; ++i) array.push(alphabet[i]);
    
    alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.split('');
    for (i=0; i<alphabet.length; ++i) array.push(alphabet[i]);
    
    var number = '0123456789'.split('');
    for (i=0; i<number.length; ++i) array.push(number[i]);
        
        
    var special = '!@#$%^&*(){}][:;><,.]'.split('');
    for (i=0; i<special.length; ++i) array.push(special[i]);
    
        
    var pass = "";
    for (i=0; i<16; ++i) {
        pass += array[Math.floor(Math.random() * array.length)];
    } 
    
    return pass;
}


///////////////////////////////////
///////////// Common Methods
///////////////////////////////////

function doLoginRedirect(targetUrl, key, url) {
     // We need to redirect to the correct login page.
     let status = {
        status: "redirect",
        key: key,
        url: url
    }
    const storeKey = "clearid/" + targetUrl +  "/status";
    console.log("Storing status at " + storeKey, status);
    localStorage.setItem(storeKey, JSON.stringify(status));
    chrome.tabs.query({currentWindow: true, active: true}, function (tab) {
        // Redirects are tricky since the entire domain can even change... Store the tab id
        // then we can try and use that on an initial status get.
        chrome.tabs.update(tab.id, {url: 'http://' + url});
    });
}

$(document).on('click', '.put_password', function(e) {
    
    let key = $(this).attr('data-user');
    let url = $(this).attr('data-url');
    $(this).addClass('spinner-grow').addClass('text-primary').find('i').hide();    
    var targetUrl = getBaseDomain(url);

    // See if we need to redirect to another domain than the current tab.
    chrome.runtime.sendMessage({from: "popup", subject: "TabInfo"}, function(response) {
        if (response.success) {
            if (getBaseDomain(response.hostname) == targetUrl) {
                 // We need to inject the script to help the content scripts find the login page.
                chrome.tabs.executeScript(sender.tab.id, {file: 'js/jquery.js'}, function() {
                    chrome.tabs.executeScript(sender.tab.id, {file: 'js/pwchange.js'}, function() {
                        // Send a message to indicate that the script has been executed successfully.
                        chrome.runtime.sendMessage({from: "navigator", subject: "navigateLogin", key: key, url: url});
                    });
                });
            } else {
                doLoginRedirect(targetUrl, key, url);
            }
        } else {
            doLoginRedirect(targetUrl, key, url);
        }
    }); 

    e.preventDefault();
    return false;
});

// edit credential
$(document).on('click', '.edit_credential', function(e){
    e.preventDefault();
    
    id = $(this).attr('data-user');
    url = $(this).attr('data-url');

    window.location = "edit_credential.html?id="+id+"&url="+url;
    return false;
});

// delete credential
$(document).on('click', '.delete_credential', function(e){
    e.preventDefault();
    
    user = $(this).attr('data-user');
    url = $(this).attr('data-url');

    var conf = confirm("Are you sure want to delete?");

    if (conf) {
        window.location = "delete_credential.html?id="+user+"&url="+url;
    }
    return false;
});


// Toggle Password
$(document).on('click', '.toggle_password_field', function(e){
    e.preventDefault();
    
    if ($(this).find('i').hasClass('fa-eye-slash')) {
        $(this).find('i').removeClass('fa-eye-slash').addClass('fa-eye');
        $(".password").attr('type', 'text');
    
    } else {
        $(this).find('i').removeClass('fa-eye').addClass('fa-eye-slash');
        $(".password").attr('type', 'password');
    }
});

// Method to get prams from URL
var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
};

// remove subdomain from a given URL
var remove_sub_domain=function(v) {
    var is_co=v.match(/\.co\./)
    v=v.split('.')
    v=v.slice(is_co ? -3: -2)
    v=v.join('.')
    console.log(v)
    return v;
}


///////////////////////////////////
///////////// QR CODE PAGE SCRIPTS
///////////////////////////////////

function get_new_qrcode()
{
    $.ajax({
        url: api_url + 'passwordless/pair',
        type: 'get',
        dataType: 'json',
        async: false,
        crossDomain: 'true',

        success: function(data, status) {
            console.log("Status: "+status+"\nData: "+data);
            
            $("#clear_id_qr_code").attr('src', 'data:image/png;base64,' + data.qrcode);
            $("#clear_id_qr_code").attr('data-challenge', data.challenge);

            setTimeout(function() {
                get_did();
            }, 20000);
        },
        error: function(xhr, status, err) {
            var error = JSON.parse(xhr.responseText);
            show_message('error', "Server Error!", error.message);
        } 
    });
}

function get_did()
{
    // first get the latest challange from the QR code 
    var challenge = $("#clear_id_qr_code").attr('data-challenge');

    // Now call the API to get DID from challange
    $.ajax({
        url: api_url + 'passwordless/get-did',
        data: { 
            challenge: challenge, 
        },
        type: 'get',
        dataType: 'json',
        async: false,
        crossDomain: 'true',

        success: function(data, status) {
            if (data.did) {
                localStorage.setItem('did', data.did);
                localStorage.setItem('token', data.token);
                window.location.href = "pair-success.html";
            }   
        },
        error: function(xhr, status, err) {
            var error = JSON.parse(xhr.responseText);
            show_message('error', "Server Error!", error.message);
        }
    });
}
function get_password(url, key, requester, do_return =false, callback =false)
{
    $.ajax({
        url: api_url + 'passwordless/password',
        type: 'get',
        dataType: 'json',
        async: true,
        crossDomain: 'true',
        data: { 
            url: url, 
            key: key, 
            token: localStorage.getItem('token'), 
            did: localStorage.getItem('did'), 
        },

        success: function(data, status) {

            hide_loading();
            
            console.log("Password Received:");
            console.log(data);

            var user = "";
            var password = "";

            credential = decryptCredential(data);
            console.log(credential);

            if (callback) {
                window[callback](credential);
            }

            if (do_return) {
                return credential;
            }

            // set callback function
            const fillPasswordFinished = res => {

                // after fill the form close the window
                window.close();
            };
            
            // get the active tab first
            chrome.tabs.query({
                active: true,
                currentWindow: true
            }, tabs => {
                // Re-encrypt the password so that only the intended website can decrypt it.
                let derived = getCurrentAppKey(requester);
                credential = encryptCredential(credential, derived);
                let tabId = tabs[0].id;
                let statusKey = "clearid/" + requester +  "/status";
                localStorage.removeItem(statusKey);

                chrome.tabs.executeScript(tabId, {file: 'js/sodium.js'}, function() {
                    chrome.tabs.executeScript(tabId, {file: 'js/sodium-page.js'}, function() {
                        // send a request to fill password into form
                        chrome.tabs.sendMessage(
                            tabId,
                            {from: 'popup', subject: 'FillPassword', credential: credential},
                            // ...also specifying a callback to be called 
                            //    from the receiving end (content script).
                            fillPasswordFinished
                        );
                    });
                });                
            });
        },
        error: function(xhr, status, err) {
            hide_loading();
            var error = JSON.parse(xhr.responseText);
            show_message('error', "Server Error!", error.message);
        } 
    });
}

function get_secure_detail(url, id, category, callback =false)
{
    var fieldSets = [];
    var string = url + "://" + id;
    fieldSets.push(string);
    var data = { 
            "url": url,
            "fieldSets": fieldSets,
            "category": category,
            "token": localStorage.getItem('token'), 
            "did": localStorage.getItem('did'), 
    };

    data = JSON.stringify(data);
    $.ajax({
        url: api_url + 'passwordless/form',
        type: 'post',
        dataType: 'json',
        contentType: 'application/json',
        async: true,
        crossDomain: 'true',
        data: data,

        success: function(data, status) {
            hide_loading();
            console.log("Data Received: ");
            //var matches = vaultDecrypt(data.matches[0]);
            console.log(data);

            if (callback) {
                window[callback](data);
            }
        },
        error: function(xhr, status, err) {
            hide_loading();
            var error = JSON.parse(xhr.responseText);
            show_message('error', "Server Error!", error.message);
        } 
    });
}

// API call to delete credential
function delete_credential(url, key, category, callback =false)
{
    var data = { 
            url: url, 
            key: key,
            category: category, 
            token: localStorage.getItem('token'), 
            did: localStorage.getItem('did'), 
        };
    var token = localStorage.getItem('token');
    var did = localStorage.getItem('did');
    var params = "url="+url+"&key="+key+"&category="+category+"&token="+token+"&did="+did;

    data = JSON.stringify(data);

    $.ajax({
        url: api_url + 'passwordless/remove?'+params,
        type: 'delete',
        dataType: 'json',
        async: true,
        crossDomain: 'true',
        data: data,

        success: function(data, status) {

            hide_loading();
            console.log("Credential deleted");

            if (callback) {
                window[callback](data);
            }
        },
        error: function(xhr, status, err) {
            hide_loading();
            var error = JSON.parse(xhr.responseText);
            show_message('error', "Server Error!", error.message);
        } 
    });
}


function fetch_vault_keys(callback =false)
{
    console.log("==>>");     
    console.log(localStorage.getItem('token'));   
    console.log(localStorage.getItem('did'));   
    console.log("==>>");   
    
    $.ajax({
        url: api_url + 'passwordless/vault-keys',
        data: {
            token: localStorage.getItem('token'), 
            did: localStorage.getItem('did'), 
        },
        type: 'get',
        dataType: 'json',
        async: false,
        crossDomain: 'true',

        success: function(data, status) {

            hide_loading();
            var credentials = data.credential;
            var creditCards = data.creditCard;
            var addresses = data.address;
            var notes = data.secureNote;
            localStorage.setItem('credentials', JSON.stringify(credentials));
            localStorage.setItem('creditCards', JSON.stringify(creditCards));
            localStorage.setItem('addresses', JSON.stringify(addresses));
            localStorage.setItem('notes', JSON.stringify(notes));

            if (callback) {
                window[callback](data);
            }
        },
        error: function(xhr, status, err) {

            hide_loading();
            var error = JSON.parse(xhr.responseText);
            show_message('error', "Server Error!", error.message);
            show_message('info', "Error!", "Please do logout and try again otherwise notifications will not work");
        }
    });
}

/////////////////////////////////////////////////////////////////
//////////////////// Top Search Functions
/////////////////////////////////////////////////////////////////


var timer;

$(function() {
    $(document).on('keyup', '#search', function() {
        clearTimeout(timer);
        timer = setTimeout(function (event) {

            console.log('Search keypress');
            perform_search();
        }, 200);
    });
});

function perform_search()
{
    var keyword = $("#search").val();
    if (!keyword) {
        $(".search_result").remove();
        return false;
    }

    if ($('.main-wrapper').find('.search_result').length < 1) {

        $('.main-wrapper').append('<div class="search_result"><div class="repeat-bx-content"></div></div>');
    }

    var $search_result_container = $(".search_result .repeat-bx-content");


    var html = "";

    /// first search in credentials
    var credentials = JSON.parse(localStorage.getItem('credentials'));

    var total_credentials = 0;

    
    $.each(credentials, function(key, domain) {

        $.each(domain.entries, function(k, entry) {

            // match the keyword against values
            if ((entry.hint.toLowerCase().indexOf(keyword.toLowerCase()) != -1) || (domain.url.toLowerCase().indexOf(keyword.toLowerCase()) != -1)) {

                total_credentials = total_credentials + 1;
                html = html + '<div class="repeat-bx-content-row">'  
                            + '<div class="row align-items-center">' 
                            +     '<div class="col google-icon"><div class="icon"><img src="http://favicon.yandex.net/favicon/'+domain.url+'" alt="icon"></div></div>'
                            +         '<div class="col-6"><div class="account-detail"><h2>'+entry.hint+'</h2><p>'+domain.url+'</p></div></div>' 
                            +         '<div class="col"><div class="actions-btns">'
                            +               '<span class="edit_credential" data-user="'+entry.key+'" data-url="'+domain.url+'"><i class="fas fa-edit"></i></span>'
                            +               '<span class="delete_credential" data-user="'+entry.key+'" data-url="'+domain.url+'"><i class="fas fa-trash"></i></span>'
                            +               '<span class="put_password" data-user="'+entry.key+'" data-url="'+domain.url+'"><i class="fas fa-key"></i></span>'
                            +         '</div></div>'
                            +     '</div>'
                            + '</div>';
            }
           
        });
    });


    // search in cards
    var cards = JSON.parse(localStorage.getItem('creditCards'));
    
    var total_cards = 0;
    
    $.each(cards, function(key, domain) {

        $.each(domain.entries, function(k, entry) {

            // match the keyword against values
            if ((entry.hint.toLowerCase().indexOf(keyword.toLowerCase()) != -1) || (domain.url.toLowerCase().indexOf(keyword.toLowerCase()) != -1)) {

                total_cards = total_cards + 1;
                html = html + '<div class="repeat-bx-content-row">'  
                                + '<div class="row align-items-center">' 
                                +     '<div class="col google-icon"><div class="icon"><img src="http://favicon.yandex.net/favicon/'+domain.url+'" alt="icon"></div></div>'
                                +         '<div class="col-6"><div class="account-detail"><h2> **** **** **** '+entry.hint+'</h2><p>'+domain.url+'</p></div></div>' 
                                +         '<div class="col"><div class="actions-btns">'
                                +               '<span class="edit_card" data-id="'+entry.key+'" data-url="'+domain.url+'"><i class="fas fa-edit"></i></span>'
                                +               '<span class="delete_card" data-id="'+entry.key+'" data-url="'+domain.url+'"><i class="fas fa-trash"></i></span>'
                              
                                +         '</div></div>'
                                +     '</div>'
                                + '</div>';
            }
        });
    });

    // search in Address
    var addresses = JSON.parse(localStorage.getItem('addresses'));
    
    var total_addresses = 0;
    
    $.each(addresses, function(key, domain) {

        $.each(domain.entries, function(k, entry) {

            // match the keyword against values
            if ((entry.hint.toLowerCase().indexOf(keyword.toLowerCase()) != -1) || (domain.url.toLowerCase().indexOf(keyword.toLowerCase()) != -1)) {

                total_addresses = total_addresses + 1;
                html = html + '<div class="repeat-bx-content-row">'  
                                + '<div class="row align-items-center">' 
                                +     '<div class="col google-icon"><div class="icon"><img src="icons/address.png" alt="icon"></div></div>'
                                +         '<div class="col-6"><div class="account-detail"><h2>'+entry.hint+'</h2><p>'+domain.url+'</p></div></div>' 
                                +         '<div class="col"><div class="actions-btns">'
                                +               '<span class="edit_address" data-id="'+entry.key+'" data-url="'+domain.url+'"><i class="fas fa-edit"></i></span>'
                                +               '<span class="delete_address" data-id="'+entry.key+'" data-url="'+domain.url+'"><i class="fas fa-trash"></i></span>'
                              
                                +         '</div></div>'
                                +     '</div>'
                                + '</div>';
            }
        });
    });

    // search in Notes
    var notes = JSON.parse(localStorage.getItem('notes'));
    
    var total_notes = 0;
    
    $.each(notes, function(key, domain) {

        $.each(domain.entries, function(k, entry) {

            // match the keyword against values
            if ((entry.hint.toLowerCase().indexOf(keyword.toLowerCase()) != -1) || (domain.url.toLowerCase().indexOf(keyword.toLowerCase()) != -1)) {

                total_notes = total_notes + 1;
                html = html + '<div class="repeat-bx-content-row">'  
                                + '<div class="row align-items-center">' 
                                +     '<div class="col google-icon"><div class="icon"><img src="icons/notes.png" alt="icon"></div></div>'
                                +         '<div class="col-6"><div class="account-detail"><h2>'+entry.hint+'</h2><p>'+domain.url+'</p></div></div>' 
                                +         '<div class="col"><div class="actions-btns">'
                                +               '<span class="edit_note" data-id="'+entry.key+'" data-url="'+domain.url+'"><i class="fas fa-edit"></i></span>'
                                +               '<span class="delete_note" data-id="'+entry.key+'" data-url="'+domain.url+'"><i class="fas fa-trash"></i></span>'
                               
                                +         '</div></div>'
                                +     '</div>'
                                + '</div>';
            }
        });
    });

    $search_result_container.html(html);
}

function show_message(type ='success', title ='title', message ='message', callback =false)
{
    $.toast({
        heading: title,
        text: message,
        showHideTransition: 'slide',
        position: {
            left: 0,
            bottom: 0
        },
        hideAfter: 5000,
        icon: type
    });
}

function get_uuidv4() 
{
    return ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g, c =>
        (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
    );
}

function show_loading()
{
    var html = '<div class="ajax_wait"><p class="message">You must receive an notification on your ClearID mobile, please approve it to use it in extension...</p></div>';
    $('body').append(html);
    
    $(".ajax_wait").show();
}
function hide_loading()
{
    $(".ajax_wait").remove();
}

$(function(){

    // Handle enter click on add form 
    $('#add_form').keypress(function(event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        
        if(keycode == '13'){
            $("#form_add").trigger('click');
        }
    });
});

// hide "open in new tab" if extension is already in new tab
chrome.tabs.query({'active': true, 'lastFocusedWindow': true}, function (tabs) {
    var url = tabs[0].url;

    if (url.indexOf(chrome.runtime.id) > -1) {
        $(".new_tab_link").hide();

        // add a separate class on body to manage extra CSS if page is on full screen
        $("body").addClass('full_page');
    }
});