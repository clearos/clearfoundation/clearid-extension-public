var api_url = "https://decentralized.clearfoundation.com/fido/";

if (!localStorage.getItem('did')) {
    window.location.href = "qr_code.html";

} else {
    // at the SYNC success time, fetch all the saved credentials to show the users
    fetch_vault_keys('callback_fetch_valut_keys');
}


function callback_fetch_valut_keys(data) 
{
    // now redirect page to my vault page
    setTimeout(function() {
        window.location.href = "my-vault-page.html";
    }, 2000);
}