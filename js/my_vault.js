var api_url = "https://decentralized.clearfoundation.com/fido/";

// This script is allowed only if user logged in. If not redirect to QR Code page
if (!localStorage.getItem('did')) {
    window.location.href = "qr_code.html";
}

////////////////////////////////////////////
/// start my vault page scripts //
console.log("==>>");     
console.log(localStorage.getItem('token'));   
console.log(localStorage.getItem('did'));   
console.log("==>>");   

function load_localstorage_credintials()
{
    var domains = JSON.parse(localStorage.getItem('credentials'));
    console.log(domains);
    
    var total_credentials = 0;
    $(".credential_list").html('');
    
    $.each(domains, function(key, domain) {
        $.each(domain.entries, function(k, entry) {

            
            total_credentials = total_credentials + 1;
            var html = '<div class="repeat-bx-content-row">'  
                        + '<div class="row align-items-center">' 
                        +     '<div class="col google-icon"><div class="icon"><img src="http://favicon.yandex.net/favicon/'+domain.url+'" alt="icon"></div></div>'
                        +         '<div class="col-6"><div class="account-detail"><h2>'+entry.hint+'</h2><p>'+domain.url+'</p></div></div>' 
                        +         '<div class="col"><div class="actions-btns">'
                        +               '<span><a class="visit_link" href="http://'+domain.url+'" target="_blank"><img src="images/share.png"></a></span>'
                        +               '<span class="edit_credential" data-user="'+entry.key+'" data-url="'+domain.url+'"><img src="images/edit-button.png"></span>'
                        +               '<span class="delete_credential" data-user="'+entry.key+'" data-url="'+domain.url+'"><img src="images/delete.png"></span>'
                        +               '<span class="put_password" data-user="'+entry.key+'" data-url="'+domain.url+'"><img src="images/key.png"></span>'
                        +         '</div></div>'
                        +     '</div>'
                        + '</div>';
            $(".credential_list").append(html);
            $(".total_credentials").text(total_credentials);
           
        });
    });

    // check if there is no record
    setTimeout(function(){

        if (total_credentials < 1) {
            $(".credential_list").append('<div class="no-record">Sorry! No saved user found</div>');
        }
    },200);
}

// Event to change form type
$("#add_form_type").change(function(){
    fix_add_form_fields();
});

// Save btn Handle click add form
$("#form_add").click(function(){
    $("#add_form").submit();
});

// Save btn Handle click edit form
$("#submit_btn").click(function(){
    $("#edit_form").submit();
})

// this method can be used to fix add form fields
function fix_add_form_fields()
{
    var type = $("#add_form_type").find("option:selected").val();
    
    if (type == "Credential") {
        $(".type_fields").hide();
        $(".credential_fields").show();

    } else if (type == "Card") {
        $(".type_fields").hide();
        $(".card_fields").show();
   
    } else if (type == "Address") {
        $(".type_fields").hide();
        $(".address_fields").show();
    
    } else if (type == "Note") {
        $(".type_fields").hide();
        $(".note_fields").show();
    }
}

// Form Handler when click on save button on add form
$("#add_form").on("submit", function (event) {
    event.preventDefault();
    console.log("Form Submitted");

    var type = $("#add_form_type").find("option:selected").val();

    if (type == "Credential") {

        var name = $("#add_form .name").val();
        var user = $("#add_form .user").val();
        var password = $("#add_form .password").val();
        var url = $("#add_form .url").val();

        var errors = [];

        if (!user)
            errors.push('Please fill the username');

        if (!password)
            errors.push('Please fill the password');

        if (!url)
            errors.push('Please fill the URL');

        if (errors.length > 0) {
            show_message('error', 'Error', errors);
        } 


        if (user && password && url) {
            add_credential(name, user, password, url);
        }
    
    } else if (type == "Card") {
        
        var card_holder_name = $("#add_form .card_holder_name").val();
        var card_number = $("#add_form .card_number").val();
        var card_brand = $("#add_form .card_brand").val();
        var card_expiration_month = $("#add_form .card_expiration_month").val();
        var card_expiration_year = $("#add_form .card_expiration_year").val();
        var card_cvv = $("#add_form .card_cvv").val();

        var errors = [];

        if (!card_holder_name)
            errors.push('Please fill the card holder name');

        if (!card_number)
            errors.push('Card number is not correct');

        if (!card_expiration_month)
            errors.push('Please fill the expiration Month');

        if (!card_expiration_year)
            errors.push('Please fill the expiration Year');

        if (!card_cvv)
            errors.push('Please fill the CVV Number');

        // now show the error else submit the form
        if (errors.length > 0) {
            show_message('error', 'Error', errors);
        
        } 

        if (card_holder_name && card_number && card_brand && card_expiration_month && card_expiration_year && card_cvv) {
            add_card(card_holder_name, card_number, card_brand, card_expiration_month, card_expiration_year, card_cvv);
        }

    } else if (type == "Address") {
        
        var address_locality = $("#add_form .address_locality").val();
        var address_region = $("#add_form .address_region").val();
        var address_postal_code = $("#add_form .address_postal_code").val();
        var address_street_address = $("#add_form .address_street_address").val();
        var address_post_office_box_number = $("#add_form .address_post_office_box_number").val();
        var address_country = $("#add_form .address_country").val();

        var errors = [];

        if (!address_locality)
            errors.push('Please fill the Locality');

        if (!address_region)
            errors.push('Please fill the Region');

        if (!address_postal_code)
            errors.push('Please fill the Postal Code');

        if (!address_street_address && !address_post_office_box_number)
            errors.push('Please fill either Street Address or PO Box No');

        if (address_street_address && address_post_office_box_number)
            errors.push('Please fill either Street Address or PO Box No');

        // now show the error else submit the form
        if (errors.length > 0) {
            show_message('error', 'Error', errors);
        
        }

        if (errors.length < 1) {
            add_address(address_locality, address_region, address_postal_code, address_street_address, address_post_office_box_number, address_country);
        }
    
    } else if (type == "Note") {
        
        var note_title = $("#add_form .note_title").val();
        var note_note = $("#add_form .note_note").val();
        var note_version = "";
        var note_created = "";

        var errors = [];

        if (!note_title)
            errors.push('Please fill the Title');

        if (!note_note)
            errors.push('Please fill the Note');

        // now show the error else submit the form
        if (errors.length > 0) {
            show_message('error', 'Error', errors);
        
        }
    
        if (errors.length < 1) {
            add_note(note_title, note_note, note_version, note_created);
        }
    }
    return false;
});

// Handler for Update form 
$("#edit_form").on("submit", function (event) {
    event.preventDefault();
    console.log("Form Submitted");

    var name = "Test";
    var user = $("#edit_form .user").val();
    var password = $("#edit_form .password").val();
    var url = $("#edit_form .url").val();

    var errors = [];

    if (!user)
        errors.push('Please fill the username');

    if (!password)
        errors.push('Please fill the password');

    if (!url)
        errors.push('Please fill the URL');

    if (errors.length > 0) {
        show_message('error', 'Error', errors);
    } 


    if (user && password && url) {
        add_credential(name, user, password, url);
    }

    return false;
});

function add_credential(name, user, password, url)
{
    $("#form_add").text("wait...");
    var fields = make_fields(user, password);
    
    fields = vaultCrypt({"fields": fields});
    
    var fieldSets = make_field_sets(fields.fields, user);

    var data = {
            'fieldSets': fieldSets,
            'token': localStorage.getItem('token'), 
            'did': localStorage.getItem('did'),
            'category': "credential",
            //'key': get_uuidv4(),
            'url': url
        }
    data = JSON.stringify(data);
    $.ajax({
        url: api_url + 'passwordless/store',
        data: JSON.stringify(data),
        type: 'post',
        dataType: 'json',
        contentType: 'application/json',
        async: false,
        crossDomain: 'true',

        success: function(data, status) {
            console.log('stored success');
            fetch_vault_keys('callback_delete_credential_refresh');
        },
        error: function(xhr, status, err) {
            $("#form_add").text("Save");
            var error = JSON.parse(xhr.responseText);
            show_message('error', "Server Error!", error.message);
        }
    });
}

function add_card(card_holder_name, card_number, card_brand, card_expiration_month, card_expiration_year, card_cvv, url ="clearunited.com")
{
    $("#form_add").text("wait...");
    var fields = make_fields_card(card_holder_name, card_number, card_brand, card_expiration_month, card_expiration_year, card_cvv);
    fields = vaultCrypt({"fields": fields});
    var lastFour = card_number.substr(card_number.length - 4);
    var fieldSets = make_field_sets(fields.fields, lastFour, true);

    console.log(fieldSets);

    var data = {
            'fieldSets': fieldSets,
            'token': localStorage.getItem('token'), 
            'did': localStorage.getItem('did'),
            'category': "creditCard",
            'url': url
        }
    data = JSON.stringify(data);
    $.ajax({
        url: api_url + 'passwordless/store',
        data: JSON.stringify(data),
        type: 'post',
        dataType: 'json',
        contentType: 'application/json',
        async: false,
        crossDomain: 'true',

        success: function(data, status) {
            console.log('stored success');
            fetch_vault_keys('callback_delete_card_refresh');
        },
        error: function(xhr, status, err) {
            $("#form_add").text("Save");
            var error = JSON.parse(xhr.responseText);
            show_message('error', "Server Error!", error.message);
        }
    });
}
function add_address(locality, region, postal_code, street_address, post_office_box_number, country, url ="clearunited.com")
{
    $("#form_add").text("wait...");
    var fields = make_fields_address(locality, region, postal_code, street_address, post_office_box_number, country);

    fields = vaultCrypt({"fields": fields});

    var fieldSets = make_field_sets(fields.fields, locality, true);

    console.log(fieldSets);

    var data = {
            'fieldSets': fieldSets,
            'token': localStorage.getItem('token'), 
            'did': localStorage.getItem('did'),
            'category': "address",
            'url': url
        }
    data = JSON.stringify(data);
    $.ajax({
        url: api_url + 'passwordless/store',
        data: JSON.stringify(data),
        type: 'post',
        dataType: 'json',
        contentType: 'application/json',
        async: false,
        crossDomain: 'true',

        success: function(data, status) {
            console.log('stored success');
            fetch_vault_keys('callback_delete_address_refresh');
        },
        error: function(xhr, status, err) {
            $("#form_add").text("Save");
            var error = JSON.parse(xhr.responseText);
            show_message('error', "Server Error!", error.message);
        }
    });
}

function add_note(title, note, version, created, url ="clearunited.com")
{
    $("#form_add").text("wait...");

    var fields = make_fields_note(title, note, version, created);
    fields = vaultCrypt({"fields": fields});
    var fieldSets = make_field_sets(fields.fields, title, true);
    console.log(fieldSets);

    var data = {
        'fieldSets': fieldSets,
        'token': localStorage.getItem('token'),
        'did': localStorage.getItem('did'),
        'category': "secureNote",
        'url': url
    }

    data = JSON.stringify(data);
    
    $.ajax({
        url: api_url + 'passwordless/store',
        data: JSON.stringify(data),
        type: 'post',
        dataType: 'json',
        contentType: 'application/json',
        async: false,
        crossDomain: 'true',

        success: function(data, status) {
            console.log('stored success');
            fetch_vault_keys('callback_delete_note_refresh');
        },
        error: function(xhr, status, err) {
            $("#form_add").text("Save");
            var error = JSON.parse(xhr.responseText);
            show_message('error', "Server Error!", error.message);
        }
    });
}

function make_field_sets(fields, hint = "", global =false)
{
    fieldSets = [];

    fieldSet = {};

    fieldSet['fields'] = fields;
    fieldSet['hint'] = hint;
    fieldSet['global'] = global;

    fieldSets.push(fieldSet);
    return fieldSets;
}
function make_fields(user, password)
{
    fields = [];

    if (user) {
        userF = {};
        userF['name'] = 'user';
        userF['value'] = user;
        userF['global'] = false;
        fields.push(userF);
    }

    if (password) {
        passF = {};
        passF['name'] = 'password';
        passF['value'] = password;
        passF['global'] = false;
        fields.push(passF);
    }
    return fields;
}
function make_fields_card(name, number, brand, expirationMonth, expirationYear, cvv)
{
    fields = [];

    if (name) {
        userF = {};
        userF['name'] = 'name';
        userF['value'] = name;
        userF['global'] = false;
        fields.push(userF);
    }

    if (number) {
        passF = {};
        passF['name'] = 'number';
        passF['value'] = number;
        passF['global'] = false;
        fields.push(passF);
    }

    if (expirationMonth) {
        passF = {};
        passF['name'] = 'expirationMonth';
        passF['value'] = expirationMonth;
        passF['global'] = false;
        fields.push(passF);
    }

    if (expirationYear) {
        passF = {};
        passF['name'] = 'expirationYear';
        passF['value'] = expirationYear;
        passF['global'] = false;
        fields.push(passF);
    }

    if (cvv) {
        passF = {};
        passF['name'] = 'cvv';
        passF['value'] = cvv;
        passF['global'] = false;
        fields.push(passF);
    }
    return fields;
}
function make_fields_address(addressLocality, addressRegion, postalCode, streetAddress, postOfficeBoxNumber, addressCountry)
{
    fields = [];

    if (addressLocality) {
        userF = {};
        userF['name'] = 'addressLocality';
        userF['value'] = addressLocality;
        userF['global'] = false;
        fields.push(userF);
    }

    if (addressRegion) {
        passF = {};
        passF['name'] = 'addressRegion';
        passF['value'] = addressRegion;
        passF['global'] = false;
        fields.push(passF);
    }

    if (postalCode) {
        passF = {};
        passF['name'] = 'postalCode';
        passF['value'] = postalCode;
        passF['global'] = false;
        fields.push(passF);
    }

    if (streetAddress) {
        passF = {};
        passF['name'] = 'streetAddress';
        passF['value'] = streetAddress;
        passF['global'] = false;
        fields.push(passF);
    }

    if (postOfficeBoxNumber) {
        passF = {};
        passF['name'] = 'postOfficeBoxNumber';
        passF['value'] = postOfficeBoxNumber;
        passF['global'] = false;
        fields.push(passF);
    }

    if (addressCountry) {
        passF = {};
        passF['name'] = 'addressCountry';
        passF['value'] = addressCountry;
        passF['global'] = false;
        fields.push(passF);
    }
    return fields;
}
function make_fields_note(title, note, version, created)
{
    fields = [];

    if (title) {
        userF = {};
        userF['name'] = 'title';
        userF['value'] = title;
        userF['global'] = false;
        fields.push(userF);
    }

    if (note) {
        passF = {};
        passF['name'] = 'note';
        passF['value'] = note;
        passF['global'] = false;
        fields.push(passF);
    }

    if (version) {
        passF = {};
        passF['name'] = 'version';
        passF['value'] = version;
        passF['global'] = false;
        fields.push(passF);
    }

    if (created) {
        passF = {};
        passF['name'] = 'created';
        passF['value'] = created;
        passF['global'] = false;
        fields.push(passF);
    }

    return fields;
}

if ($(".credential_list").length > 0) {
    load_localstorage_credintials();
}

// When the add form is open
if ($("#add_form").length > 0) {

    var type = getUrlParameter('type');
    
    if (type == "Credential") {
        $("#add_form_type").val(type);

    } else if (type == "Card") {
        $("#add_form_type").val(type);
        
    } else if (type == "Address") {
        $("#add_form_type").val(type);
        
    } else if (type == "Note") {
        $("#add_form_type").val(type);
    }

    fix_add_form_fields();
}

/// end my vault page scripts //
////////////////////////////////////////////



/// Start Edit Credentials form scripts //
///////////////////////////////////////////

$(function() {
    
    if ($(".edit_form").length > 0) {
        var id = getUrlParameter('id');
        var url = getUrlParameter('url');

        show_loading();
        get_password(url, id, true, 'callback_fill_edit_form');

        $("#edit_form .url").val(url);
    }


    if ($(".delete_form").length > 0) {
        var id = getUrlParameter('id');
        var url = getUrlParameter('url');

        show_loading();
        delete_credential(url, id, 'credential', 'callback_delete_credential');
    }

});



function callback_fill_edit_form(data)
{
    $("#edit_form .user").val(data.user);
    $("#edit_form .password").val(data.password);
}
function callback_delete_credential(data)
{
    fetch_vault_keys('callback_delete_credential_refresh');
}
function callback_delete_credential_refresh(data)
{
    window.location.href = "my_credentials.html";
}


//// Start Credit Card Page Script //
/////////////////////////////////////////////

if ($(".card_list").length > 0) {
    load_localstorage_cards();
}

function load_localstorage_cards()
{
    var cards = JSON.parse(localStorage.getItem('creditCards'));
    console.log(cards);
    
    var total_cards = 0;
    $(".card_list").html('');
    
    $.each(cards, function(key, domain) {

        console.log(domain.entries);

        $.each(domain.entries, function(k, entry) {

                total_cards = total_cards + 1;
                var html = '<div class="repeat-bx-content-row">'  
                            + '<div class="row align-items-center">' 
                            +     '<div class="col google-icon"><div class="icon"><img src="icons/card.png" alt="icon"></div></div>'
                            +         '<div class="col-6"><div class="account-detail"><h2> **** **** **** '+entry.hint+'</h2><p>'+domain.url+'</p></div></div>' 
                            +         '<div class="col"><div class="actions-btns">'
                            +               '<span class="edit_card" data-id="'+entry.key+'" data-url="'+domain.url+'"><img src="images/edit-button.png"></span>'
                            +               '<span class="delete_card" data-id="'+entry.key+'" data-url="'+domain.url+'"><img src="images/delete.png"></span>'
                            +         '</div></div>'
                            +     '</div>'
                            + '</div>';
                $(".card_list").append(html);
                $(".total_cards").text(total_cards);
        });
    });

    // check if there is no record
    setTimeout(function(){

        if (total_cards < 1) {
            $(".card_list").append('<div class="no-record">Sorry! No saved card found</div>');
        }
    }, 200);
}


// edit credential
$(document).on('click', '.edit_card', function(e){
    e.preventDefault();
    
    var id = $(this).attr('data-id');
    url = $(this).attr('data-url');

    window.location = "edit_card.html?id="+id+"&url="+url;
    return false;
});

// edit credential
$(document).on('click', '.delete_card', function(e){
    e.preventDefault();
    
    var id = $(this).attr('data-id');
    url = $(this).attr('data-url');

    var conf = confirm("Are you sure want to delete?");

    if (conf) {
        window.location = "delete_card.html?id="+id+"&url="+url;
    }
    return false;
});

if ($(".edit_form_card").length > 0) {
    var id = getUrlParameter('id');
    var url = getUrlParameter('url');
    $(".card_url").val(url);

    show_loading();
    get_secure_detail(url, id, 'creditCard', 'callback_get_card_detail');
}

// Save btn Handle click edit form
$("#submit_btn_edit_card").click(function(){
    $(".edit_form_card").submit();
})

// Form Handler when click on save button on edit form
$("#edit_form_card").on("submit", function (event) {
    event.preventDefault();
    console.log("Form Submitted");
  
    var card_holder_name = $("#edit_form_card .card_holder_name").val();
    var card_number = $("#edit_form_card .card_number").val();
    var card_brand = $("#edit_form_card .card_brand").val();
    var card_expiration_month = $("#edit_form_card .card_expiration_month").val();
    var card_expiration_year = $("#edit_form_card .card_expiration_year").val();
    var card_cvv = $("#edit_form_card .card_cvv").val();
    var card_url = $("#edit_form_card .card_url").val();

    

    if (card_holder_name && card_number && card_brand && card_expiration_month && card_expiration_year && card_cvv) {
        add_card(card_holder_name, card_number, card_brand, card_expiration_month, card_expiration_year, card_cvv, card_url);
    }

    return false;
});

if ($(".delete_card_page").length > 0) {
    var id = getUrlParameter('id');
    var url = getUrlParameter('url');

    show_loading();
    delete_credential(url, id, 'creditCard', 'callback_delete_card');
}


function callback_get_card_detail(data)
{
    console.log(data);
    
    var data = vaultDecrypt(data.matches[0]);
    console.log(data);

    $(data.fields).each(function(key, value) {
        
        if (value.name == "name") {
            $(".card_holder_name").val(value.value);
        }
        if (value.name == "number") {
            $(".card_number").val(value.value);
        }
        if (value.name == "number") {
            $(".card_brand").val("None");
        }
        if (value.name == "expirationMonth") {
            $(".card_expiration_month").val(value.value);
        }
        if (value.name == "expirationYear") {
            $(".card_expiration_year").val(value.value);
        }
        if (value.name == "cvv") {
            $(".card_cvv").val(value.value);
        }
    });
}

function callback_delete_card(data)
{
    fetch_vault_keys('callback_delete_card_refresh');
}

function callback_delete_card_refresh(data)
{
    window.location.href = "my_cards.html";
}
/////////////////////////////////////////////
//// End Card Page Script //



//// Start Address Page Script //
/////////////////////////////////////////////

if ($(".address_list").length > 0) {
    load_localstorage_addresses();
}

function load_localstorage_addresses()
{
    var addresses = JSON.parse(localStorage.getItem('addresses'));
    console.log(addresses);
    
    var total_addresses = 0;
    $(".address_list").html('');
    
    $.each(addresses, function(key, domain) {

        console.log(domain.entries);

        $.each(domain.entries, function(k, entry) {

                total_addresses = total_addresses + 1;
                var html = '<div class="repeat-bx-content-row">'  
                            + '<div class="row align-items-center">' 
                            +     '<div class="col google-icon"><div class="icon"><img src="icons/address.png" alt="icon"></div></div>'
                            +         '<div class="col-6"><div class="account-detail"><h2>'+entry.hint+'</h2><p>'+domain.url+'</p></div></div>' 
                            +         '<div class="col"><div class="actions-btns">'
                            +               '<span class="edit_address" data-id="'+entry.key+'" data-url="'+domain.url+'"><img src="images/edit-button.png"></span>'
                            +               '<span class="delete_address" data-id="'+entry.key+'" data-url="'+domain.url+'"><img src="images/delete.png"></span>'
                            +         '</div></div>'
                            +     '</div>'
                            + '</div>';
                $(".address_list").append(html);
                $(".total_addresses").text(total_addresses);
        });
    });

    // check if there is no record
    setTimeout(function(){

            if (total_addresses < 1) {
                $(".address_list").append('<div class="no-record">Sorry! No saved address found</div>');
            }
    },200);
}


// edit credential
$(document).on('click', '.edit_address', function(e){
    e.preventDefault();
    
    var id = $(this).attr('data-id');
    url = $(this).attr('data-url');

    window.location = "edit_address.html?id="+id+"&url="+url;
    return false;
});

// edit credential
$(document).on('click', '.delete_address', function(e){
    e.preventDefault();
    
    var id = $(this).attr('data-id');
    url = $(this).attr('data-url');

    var conf = confirm("Are you sure want to delete?");

    if (conf) {
        window.location = "delete_address.html?id="+id+"&url="+url;
    }
    return false;
});

if ($(".edit_form_address").length > 0) {
    var id = getUrlParameter('id');
    var url = getUrlParameter('url');
    $(".address_url").val(url);

    show_loading();
    get_secure_detail(url, id, 'address', 'callback_get_address_detail');
}

// Save btn Handle click edit form
$("#submit_btn_edit_address").click(function(){
    $(".edit_form_address").submit();
})

// Form Handler when click on save button on edit form
$("#edit_form_address").on("submit", function (event) {
    event.preventDefault();
    console.log("Form Submitted Edit Address");
  
    var address_locality = $("#edit_form_address .address_locality").val();
    var address_region = $("#edit_form_address .address_region").val();
    var address_postal_code = $("#edit_form_address .address_postal_code").val();
    var address_street_address = $("#edit_form_address .address_street_address").val();
    var address_post_office_box_number = $("#edit_form_address .address_post_office_box_number").val();
    var address_country = $("#edit_form_address .address_country").val();
    var address_url = $("#edit_form_address .address_url").val();

    if (address_locality && address_region && address_postal_code) {
        add_address(address_locality, address_region, address_postal_code, address_street_address, address_post_office_box_number, address_country, address_url);
    }

    return false;
});

if ($(".delete_form_address").length > 0) {
    var id = getUrlParameter('id');
    var url = getUrlParameter('url');

    show_loading();
    delete_credential(url, id, 'address', 'callback_delete_address');
}


function callback_get_address_detail(data)
{
    console.log(data);
    
    var data = vaultDecrypt(data.matches[0]);
    console.log(data);

    $(data.fields).each(function(key, value) {
        
        if (value.name == "addressLocality") {
            $(".address_locality").val(value.value);
        }
        if (value.name == "addressRegion") {
            $(".address_region").val(value.value);
        }
        if (value.name == "postalCode") {
            $(".address_postal_code").val(value.value);
        }
        if (value.name == "streetAddress") {
            $(".address_street_address").val(value.value);
        }
        if (value.name == "postOfficeBoxNumber") {
            $(".address_post_office_box_number").val(value.value);
        }
        if (value.name == "addressCountry") {
            $(".address_country").val(value.value);
        }
    });
}

function callback_delete_address(data)
{
    // refresh vault keys after deletion
    fetch_vault_keys('callback_delete_address_refresh');
}

function callback_delete_address_refresh(data)
{
    window.location.href = "my_addresses.html"
}

/////////////////////////////////////////////
//// End Address Page Script //



//// Start Notes Page Script //
/////////////////////////////////////////////

if ($(".note_list").length > 0) {
    load_localstorage_notes();
}

function load_localstorage_notes()
{
    var notes = JSON.parse(localStorage.getItem('notes'));
    console.log(notes);
    
    var total_notes = 0;
    $(".note_list").html('');
    
    $.each(notes, function(key, domain) {

        console.log(domain.entries);

        $.each(domain.entries, function(k, entry) {

            var title = entry['title:version'];

                total_notes = total_notes + 1;
                var html = '<div class="repeat-bx-content-row">'  
                            + '<div class="row align-items-center">' 
                            +     '<div class="col google-icon"><div class="icon"><img src="icons/notes.png" alt="icon"></div></div>'
                            +         '<div class="col-6"><div class="account-detail"><h2>'+entry.hint+'</h2><p>'+domain.url+'</p></div></div>' 
                            +         '<div class="col"><div class="actions-btns">'
                            +               '<span class="edit_note" data-id="'+entry.key+'" data-url="'+domain.url+'"><img src="images/edit-button.png"></span>'
                            +               '<span class="delete_note" data-id="'+entry.key+'" data-url="'+domain.url+'"><img src="images/delete.png"></span>'
                            +         '</div></div>'
                            +     '</div>'
                            + '</div>';
                $(".note_list").append(html);
                $(".total_notes").text(total_notes);
        });
    });

    // check if there is no record
    setTimeout(function(){

        if (total_notes < 1) {
            $(".note_list").append('<div class="no-record">Sorry! No saved notes found</div>');
        }
    },200);
}


// edit credential
$(document).on('click', '.edit_note', function(e){
    e.preventDefault();
    
    var id = $(this).attr('data-id');
    url = $(this).attr('data-url');

    window.location = "edit_note.html?id="+id+"&url="+url;
    return false;
});

// edit credential
$(document).on('click', '.delete_note', function(e){
    e.preventDefault();
    
    var id = $(this).attr('data-id');
    url = $(this).attr('data-url');

    var conf = confirm("Are you sure want to delete?");

    if (conf) {
        window.location = "delete_note.html?id="+id+"&url="+url;
    }
    return false;
});

if ($(".edit_form_note").length > 0) {
    var id = getUrlParameter('id');
    var url = getUrlParameter('url');
    $(".note_url").val(url);

    show_loading();
    get_secure_detail(url, id, 'secureNote', 'callback_get_note_detail');
}

// Save btn Handle click edit form
$("#submit_btn_edit_note").click(function(){
    $(".edit_form_note").submit();
})

// Form Handler when click on save button on edit form
$("#edit_form_note").on("submit", function (event) {
    event.preventDefault();
    console.log("Form Submitted Edit Note");
  
    var note_title = $("#edit_form_note .note_title").val();
    var note_note = $("#edit_form_note .note_note").val();

    var note_version = $("#edit_form_note .note_version").val();
    var note_created = $("#edit_form_note .note_created").val();

    var errors = [];

    if (!note_title)
        errors.push('Please fill the Title');

    if (!note_note)
        errors.push('Please fill the Note');

    if (errors.length > 0) {
        show_message('error', 'Error', errors);
    } 
    
    // If everything is error free then call the next Step
    if (errors.length < 1) {
        add_note(note_title, note_note, note_version, note_created);
    }

    return false;
});

if ($(".delete_form_note").length > 0) {
    var id = getUrlParameter('id');
    var url = getUrlParameter('url');

    show_loading();
    delete_credential(url, id, 'secureNote', 'callback_delete_note');
}


function callback_get_note_detail(data)
{
    console.log(data);
    
    var data = vaultDecrypt(data.matches[0]);
    console.log(data);

    $(data.fields).each(function(key, value) {
        
        if (value.name == "title") {
            $(".note_title").val(value.value);
        }
        if (value.name == "note") {
            $(".note_note").val(value.value);
        }
        if (value.name == "version") {
            $(".note_version").val(value.value);
        }
        if (value.name == "created") {
            $(".note_created").val(value.value);
        }
    });
}

function callback_delete_note(data)
{
    fetch_vault_keys('callback_delete_note_refresh');
}
function callback_delete_note_refresh(data)
{
    window.location.href = "my_notes.html"
}