var api_url = "https://decentralized.clearfoundation.com/fido/";
var cancelled = false;

$(document).ready(function() {
    ddlAppSelect_changed();
    $('#fileRawData').bind('change', handleClientFile);
    $('#btnImport').click(handleImportButton);
    $('#btnCancel').click(function() {
        cancelled = true;
        toggleButtons(false);
    }).hide();
    $('.progress').hide();
    $('#ddlAppSelect').bind('change', ddlAppSelect_changed);
})

function populateAppInstructions(choice) {
    var items = {
        "lastpass": [
            "Open your LastPass vault.",
            "Choose 'More Options' > 'Advanced' > 'Export'",
            "Press Ctrl + A (Cmd + A on Mac) to select all the data.",
            "Copy and Paste the contents in the box below."
        ],
        "chrome": [
            "Go to <a href='chrome://settings/passwords' target='_blank' style='color: white;'>chrome://settings/passwords</a>",
            "Click on the menu (three dots) in line with the `Saved Passwords` heading",
            "Authenticate with the password for your user account on the *computer*",
            "Choose a location to save the password export file",
            "Import the file using the `Choose File` button below"
        ],
        "1password": [
            "Open 1Password App",
            "Choose 'File' > 'Export' > 'All Items'",
            "Choose .csv under the `File Format:` drop-down list",
            "Save the CSV to your computer",
            "Import the file using the `Choose File` button below"
        ]
    }
    $("#ulInstructions").html('')
    $.each(items[choice], function(k, entry){
        var ehtml = '<li class="instructions">' + entry + '</li>';
        $("#ulInstructions").append(ehtml);
    });

    // Change the UI based on which app is being imported from.
    switch (choice) {
        case 'lastpass':
            $('#btnImport').show();
            $('#fileRawData').hide();
            $('#txtRawData').show();
            break;
        default:
            $('#btnImport').hide();
            $('#fileRawData').show();
            $('#txtRawData').hide();
            break;
    }
}

var lastPassImportCheckboxes = {}
var lastPassOptions = ["Passwords", "Secure Notes", "Addresses", "Payment Cards"];
var chromeImportCheckboxes = {}
var chromeOptions = ["Passwords"]
var onePassImportCheckboxes = {}
var onePassOptions = ["Passwords", "Secure Notes", "Identities", "Payment Cards"];
function ddlAppSelect_changed() {
    var options = {
        "lastpass": lastPassOptions,
        "chrome": chromeOptions,
        "1password": onePassOptions
    }
    var targets = {
        "lastpass": lastPassImportCheckboxes,
        "chrome": chromeImportCheckboxes,
        "1password": onePassImportCheckboxes
    }
    var choice = $('#ddlAppSelect').val();
    $("#options-repeater").html('');
    $.each(options[choice], function(k, entry){
        ekey = entry.replace(' ', '_').toLowerCase();
        targets[choice][entry] = "include_" + ekey;
        var ehtml = '<div class="repeat-bx-content-row">'
                    + '<div class="CheckBox">'
                    +   '<label for="' + ekey + '">'
                    +     '<span class="float-left">' + entry + '</span>'
                    +     '<span class="float-right"><input checked="" id="include_' + ekey + '" type="checkbox" name="' + ekey + '"></span>'
                    +   '</label>'
                    + '</div>'
                    +'</div>';
        $("#options-repeater").append(ehtml);
    });
    populateAppInstructions(choice);
}

function getMonthFromString(mon){
    var d = Date.parse(mon + "1, 2012");
    if(!isNaN(d)){
       return new Date(d).getMonth() + 1;
    }
    return -1;
}

function lastPassNoteToDict(noteExtras) {
    var pieces = noteExtras.split('\n');
    var noteDict = {};
    $.each(pieces, function(k, entry) {
        let parts = entry.split(':');
        noteDict[parts[0]] = parts.slice(1, parts.length).join(':');
    });
    return noteDict;
}

function lastPassCardMap(creditCard) {
    var mapper = {
        "name": "Name on Card",
        "expirationMonth": "Expiration Date",
        "cvv": "Security Code",
        "number": "Number",
        "cardType": "Type"
    };

    var result = {};
    $.each(mapper, function(k, v) {
        if (k == "expirationMonth") {
            let mapValues = creditCard[v].split(',');
            let month = getMonthFromString(mapValues[0]);
            result["expirationMonth"] = month.toString();
            result["expirationYear"] = mapValues[1];
        } else {
            result[k] = creditCard[v];
        }
    });
    return result;
}

function lastPassAddressMap(address) {
    var mapper = {
        "addressLocality": "City / Town",
        "addressRegion": "State",
        "postalCode": "Zip / Postal Code",
        "streetAddress": ["Address 1", "Address 2", "Address 3"],
        "addressCountry": "Country",
        "title": "Title",
        "lastName": "Last Name",
        "firstName": "First Name",
        "middleName": "Middle Name",
        "company": "Company"
    }

    var result = {};
    $.each(mapper, function(k, v) {
        if (k == "streetAddress") {
            let isPoBox = false;
            let addressValues = [];
            $.each(v, function(i, entry) {
                let mapValue = address[entry];
                if (mapValue.indexOf("PO Box") > -1) {
                    isPoBox = true;
                }
                if (mapValue != "") {
                    addressValues.push(mapValue);
                }
            });

            if (isPoBox) {
                result["postOfficeBoxNumber"] = addressValues.join(',');
            } else {
                result["streetAddress"] = addressValues.join(',');
            }
        } else {
            result[k] = address[v];
        }
    });
    return result;
}

function lastPassNoteMap(note) {
    var mapper = {
        "note": "extra",
        "title": "name"
    };

    var result = {};
    $.each(mapper, function(k, v) {
        result[k] = note[v];
    });
    return result;
}

function lastPassCredentialMap(credential) {
    var mapper = {
        "user": "username",
        "password": "password",
    };

    var result = {};
    $.each(mapper, function(k, v) {
        result[k] = credential[v];
    });
    return result;
}

function makeFields(mapDict) {
    fields = [];

    $.each(mapDict, function(k, v) {
        let ldict = {
            'name': k,
            'value': v
        };
        fields.push(ldict);
    });   
    return fields;
}

var done = 0;
var total = 1;
function setProgress(title, percentComplete) {
    if (percentComplete != null) {
        pVal = percentComplete*100;
    } else {
        pVal = (done/total*100);
    }
    $( "#progressBar" ).attr("aria-valuenow", pVal).css("width", pVal.toFixed(1) + '%').html(title + pVal.toFixed(1) + '%');
}
function resetProgressBar() {
    done = 0;
    total = 0;
    $( "#progressBar" ).attr("aria-valuenow", 0).css("width", '0%').html(done.toString() + '/' + total.toString());
}

var csvItems = 1;
var csvProgress = 0;
function timedProcessArray(items, process, callback) {
    var todo = $.makeArray(items);

    // The first call to setTimeout() creates a timer to process the first item in the array.
    setTimeout(function () {
        var start = +new Date();

        do
        {
            // Calling todo.shift() returns the first item and also removes it from the array.
            args = todo.shift();
            process(args);

        } // After processing the item, a check is made to determine whether there are more items to process and if the time hasn't exceeded the threshold of 50 milliseconds
        while (todo.length > 0 && (+new Date() - start < 50));

        if (todo.length > 0 && !cancelled)
        {
            // Because the next timer needs to run the same code as the original, arguments.callee is passed in as the first argument.
            setTimeout(arguments.callee, 5);
        }
        else
        {   //If there are no further items to process, then a callback() function is called.
            if (callback)
            {
                callback(items);
            }
        }

    }, 5);
}

var errors = [];
var progressChunks = 0;
function bulkImport(items, callback) {
    if (cancelled) {
        //Abort the request if the user asked for cancellation.
        return;
    }
    var data = {
        'imports': items,
        'token': localStorage.getItem('token'), 
        'did': localStorage.getItem('did')       
    }
    data = JSON.stringify(data);
    progressChunks = 0;
    $.ajax({
        url: api_url + 'passwordless/import',
        data: JSON.stringify(data),
        type: 'post',
        dataType: 'json',
        contentType: 'application/json',
        async: true,
        crossDomain: 'true',
        xhr: function() {
            var xhr = new window.XMLHttpRequest();
            xhr.upload.addEventListener("progress", function(evt) {
                if (evt.lengthComputable) {
                    var percentComplete = evt.loaded / evt.total;
                    setProgress("Uploading ", percentComplete);
                }
           }, false);              
    
           return xhr;
        },
        success: function(data, status) {
            done += data.ids.length;
            setProgress("Storing ", 1);
            console.log('Store success for bulk import.');
            callback(data, null);
        },
        error: function(xhr, status, err) {
            if (xhr.responseText != "") {
                errors =  xhr.responseText;
                callback(null);
            }
        }
    });
}

function showErrors() {
    if (errors != "") {
        show_message('error', "Error!!", "There were errors while importing data.");
    }
}

function testCompleted(success, errors) {
    if (done >= total) {
        toggleButtons(false);
        showErrors();
        show_message('success', "Success!!", "Data import action completed");
        $("#txtRawData").val("");

        //refresh the data
        fetch_vault_keys();
        

    } 
    else {
        alert(done.toString() + '/' + total.toString() + ' were imported.');
    }
}

function lastPassImport(valdict) {
    let addressFieldsets = [];
    let cardFieldsets = [];
    let credentialFieldsets = {};
    let noteFieldsets = [];

    setProgress("Encrypting ", 0);
    csvItems = valdict.length;
    csvProgress = 0;

    var process = function(entry) {       
        // Now, decide what kind of entry it is based on the URL prefix.
        let mapDict = {};
        var target;
        var hint = "";
        var global = false;
        var url = null;
        if (entry.url == "http://sn") {
            // This is a secure note. LastPass uses SecureNotes to store credit cards, addresses, etc.
            // Examine the `extra` attribute to see what's going on.
            let noteDict = {};
            if (entry.extra.indexOf("NoteType:Address") > -1) {
                noteDict = lastPassNoteToDict(entry.extra);
                mapDict = lastPassAddressMap(noteDict);
                target = addressFieldsets;
                hint = entry.name + ' (' + mapDict.streetAddress + ')';
                global = true;
                url = "global";
            } else if (entry.extra.indexOf("NoteType:Credit Card") > -1) {
                noteDict = lastPassNoteToDict(entry.extra);
                mapDict = lastPassCardMap(noteDict);
                target = cardFieldsets;
                let s = mapDict.number;
                hint = entry.name + ' ' + s.substring(s.length-4, s.length);
                global = true;
                url = "global";
            } else {
                // This is just a secure note.
                mapDict = lastPassNoteMap(entry);
                target = noteFieldsets;
                hint = entry.name.substring(0, 10);
                global = true;
                url = "global";
            }
        } else {
            // This is a regular username/password credential.
            mapDict = lastPassCredentialMap(entry);
            target = credentialFieldsets;
            hint = mapDict.user;
            if (entry.url.indexOf("http") > -1) {
                url = entry.url.split('/')[2];
            } else {
                url = entry.url.split('/')[0];
            }
            global = false;
        }
        // Finally, add the additional metadata needed to make the request and encrypt the fields.
        var fields = makeFields(mapDict);
        var fieldSet = {
            'fields': fields,
            'hint': hint,
            'global': global
        }
        fieldSet = vaultCrypt(fieldSet);
        if (url != "global") {
            if (url in target) {
                target[url].push(fieldSet);
            } else {
                target[url] = [fieldSet];
            }
        } else {
            target.push(fieldSet);            
        }

        csvProgress += 1;
        setProgress("Encrypting ", csvProgress/csvItems);
    };

    timedProcessArray(valdict, process, function() {
        // Since the address, credit card, and secure notes are global, we can upload them all at once.
        resetProgressBar();
        items = [];
        if(document.getElementById(lastPassImportCheckboxes["Addresses"]).checked) {
            items.push({"fieldSets": addressFieldsets, "category": 'address', "url": "global"});
            total += addressFieldsets.length; 
        }
        if(document.getElementById(lastPassImportCheckboxes["Payment Cards"]).checked) {
            items.push({"fieldSets": cardFieldsets, "category": 'creditCard', "url": "global"});
            total += cardFieldsets.length;
        }
        if(document.getElementById(lastPassImportCheckboxes["Secure Notes"]).checked) {
            items.push({"fieldSets": noteFieldsets, "category": 'secureNote', "url": "global"});
            total += noteFieldsets.length;
        }

        // But, the credentials get stored by URL, so we have to process them individually.
        if(document.getElementById(lastPassImportCheckboxes["Passwords"]).checked) {
            $.each(credentialFieldsets, function(_url, fieldSet) {
                items.push({"fieldSets": fieldSet, "category": 'credential', "url": _url})
            });
            total += Object.keys(credentialFieldsets).length;
        }

        bulkImport(items, testCompleted);
    });
}

function chromeCredentialMap(credential) {
    var mapper = {
        "user": "username",
        "password": "password",
    };

    var result = {};
    $.each(mapper, function(k, v) {
        result[k] = credential[v];
    });
    return result;
}

function chromeImport(valdict) {
    let credentialFieldsets = {};
    
    setProgress("Encrypting ", 0);
    csvItems = valdict.length;
    csvProgress = 0;

    var process = function(entry) {       
        // Chrome only provides password exports at the moment, so we don't bother with addresses,
        // cards or secure notes.
        let mapDict = {};
        var target;
        var hint = "";
        var global = false;
        var url = null;
      
        // This is a regular username/password credential.
        mapDict = chromeCredentialMap(entry);
        target = credentialFieldsets;
        hint = mapDict.user;
        url = entry.name
        global = false;

        // Finally, add the additional metadata needed to make the request and encrypt the fields.
        var fields = makeFields(mapDict);
        var fieldSet = {
            'fields': fields,
            'hint': hint,
            'global': global
        }
        fieldSet = vaultCrypt(fieldSet);
        if (url != "global") {
            if (url in target) {
                target[url].push(fieldSet);
            } else {
                target[url] = [fieldSet];
            }
        } else {
            target.push(fieldSet);            
        }
        csvProgress += 1;
        setProgress("Encrypting ", csvProgress/csvItems);
    };
    timedProcessArray(valdict, process, function() {
        // Since the address, credit card, and secure notes are global, we can upload them all at once.
        resetProgressBar();
        items = [];

        // Credentials get stored by URL, so we have to process them individually.
        if(document.getElementById(chromeImportCheckboxes["Passwords"]).checked) {
            $.each(credentialFieldsets, function(_url, fieldSet) {
                items.push({"fieldSets": fieldSet, "category": 'credential', "url": _url})
            });
            total += Object.keys(credentialFieldsets).length;
        }

        bulkImport(items, testCompleted);
    });
}

function onePassImport(valdict)
{
    // First show the process bar 
    $(".progress").show();
    setProgress("Encrypting ", .1);

    // parse the string into JSON
    var valdict = JSON.parse(valdict);

    items = [];

    $.each(valdict, function(k, entry) {       

        // Initialize the data that we needed to make fieldset
        var hint = "";
        var category = "";
        var global_flag = true;
        var url = "global";
        var title = "";
        var note = "";

        // Case 1 : If entry is for login 
        if (entry.typeName == "webforms.WebForm") {
            var user = "";
            var password = "";
            
            category = "credential";
            global_flag = false;
            url = entry.location;

            $.each(entry.secureContents.fields, function(f, field) {     

                if (field.designation == "username")
                    user = field.value;
                
                else if (field.designation == "password")
                    password = field.value;
            });

            hint = user;
            fields = make_fields(user, password);
        
        // Case 2 : If entry is for Secure Notes
        } else if (entry.typeName == "securenotes.SecureNote") {
            category = "secureNote";
            title = entry.title;
            note = entry.secureContents.notesPlain;
            hint = entry.title;

            fields = make_fields_note(title, note);
        
        // Case 3 : If entry is for Address
        } else if (entry.typeName == "identities.Identity") {
            category = "address";
            locality = entry.secureContents.address1;
            city = entry.secureContents.city;
            region = entry.secureContents.region;
            state = entry.secureContents.state;

            // If region is NULL then set it as state
            if (!region) {
                region = state;
            }
            country = entry.secureContents.country;
            zip = entry.secureContents.zip;
            
            fields = make_fields_address(city, region, zip, locality, "", country);

            if (locality && city) {
                hint = locality + " " + city;
            }

        // Case 4 : If entry is for Credit Card
        } else if (entry.typeName == "wallet.financial.CreditCard") {
            category = "creditCard";
            name = entry.secureContents.cardholder;
            number = entry.secureContents.ccnum;
            brand = entry.secureContents.type;
            expirationMonth = entry.secureContents.expiry_mm;
            expirationYear = entry.secureContents.expiry_yy;
            cvv = entry.secureContents.cvv;

            fields = make_fields_card(name, number, brand, expirationMonth, expirationYear, cvv);
            hint = number.substr(number.length - 4);
        }

        // make sure that required fields are fulfilled
        if(category && hint) {
        
            // now encrypt the data
            fields = vaultCrypt({"fields": fields});

            // make fieldset
            var fieldSets = make_field_sets(fields.fields, hint, global_flag);

            // push item in 'items' array
            items.push({"fieldSets": fieldSets, "category": category, "url": url});
        }
        
    
    });

    done = 0;
    total = items.length;

    setProgress("Uploading ", .2);

    // Finally call the upload API
    bulkImport(items, testCompleted);
}

function onePassCardMap(creditCard) {
    var mapper = {
        "name": "Name on Card",
        "expirationMonth": "Expiration Date",
        "cvv": "Security Code",
        "number": "Number",
        "cardType": "Type"
    };

    var result = {};
    $.each(mapper, function(k, v) {
        if (k == "expirationMonth") {
            let mapValues = creditCard[v].split(',');
            let month = getMonthFromString(mapValues[0]);
            result["expirationMonth"] = month.toString();
            result["expirationYear"] = mapValues[1];
        } else {
            result[k] = creditCard[v];
        }
    });
    return result;
}

function onePassAddressMap(address) {
    var mapper = {
        "addressLocality": "City / Town",
        "addressRegion": "State",
        "postalCode": "Zip / Postal Code",
        "streetAddress": ["Address 1", "Address 2", "Address 3"],
        "addressCountry": "Country",
        "title": "Title",
        "lastName": "Last Name",
        "firstName": "First Name",
        "middleName": "Middle Name",
        "company": "Company"
    }

    var result = {};
    $.each(mapper, function(k, v) {
        if (k == "streetAddress") {
            let isPoBox = false;
            let addressValues = [];
            $.each(v, function(i, entry) {
                let mapValue = address[entry];
                if (mapValue.indexOf("PO Box") > -1) {
                    isPoBox = true;
                }
                if (mapValue != "") {
                    addressValues.push(mapValue);
                }
            });

            if (isPoBox) {
                result["postOfficeBoxNumber"] = addressValues.join(',');
            } else {
                result["streetAddress"] = addressValues.join(',');
            }
        } else {
            result[k] = address[v];
        }
    });
    return result;
}

function onePassNoteMap(note) {
    var mapper = {
        "note": "extra",
        "title": "name"
    };

    var result = {};
    $.each(mapper, function(k, v) {
        result[k] = note[v];
    });
    return result;
}

function onePassCredentialMap(credential) {
    var mapper = {
        "user": "username",
        "password": "password",
    };

    var result = {};
    $.each(mapper, function(k, v) {
        result[k] = credential[v];
    });
    return result;
}

function toggleButtons(showCancel) {
    if (showCancel) {
        $('#btnImport').hide();
        $('#btnCancel').show();
        $('#fileRawData').hide();
        $('#txtRawData').hide();
        $( ".progress" ).show();
    } else {
        $('#btnCancel').hide();
        $( ".progress" ).hide();
        populateAppInstructions($('#ddlAppSelect').val());
    }
}

function handleClientFile(event) {
    var files = event.target.files;
    var file = files[0];

    var reader = new FileReader();
    reader.readAsText(file);
    reader.onload = function(event) {
      var csv = event.target.result;
      importFromString(csv);
    }
}

function importFromString(string) {

    // In case of 1Password CSV plug-in doesn't work because the string is already in JSON
    if ($('#ddlAppSelect').val() == "1password") {

        // To remove unnecessary lines I'm going to break string into lines first
        // Take a new string to add only necessary lines to it
        var new_lines = "[";
        lines = string.split("\n");

        $.each(lines, function(n, elem) {

            // make sure that line has some value
            /*if (!elem.trim())
                continue;*/

            // Check the first 3 chars of current line
            var line_start_char = elem.substring(0, 3);

            // If first 3 chars are stars that means its a line break by 1Password
            if (line_start_char == "***") {
                // Just skip the line

            // Else add this line into JSON string
            } else {
                new_lines = new_lines + elem + ',';
            }
        });

        // replace the ending/trailing commas
        new_lines = new_lines.replace(/^\,+|\,+$/g, '');

        // Close the string to make it proper JSON string
        new_lines = new_lines + "]";

        onePassImport(new_lines);

    } else {
        importCsvData(string);
    }
}

function handleImportButton() {
    var csv = $('#txtRawData').val();
    importFromString(csv);
}

function importCsvData(csv) {
    cancelled = false;
    toggleButtons(true);
    var valdict = $.csv.toObjects(csv);
    var importers = {
        "lastpass": lastPassImport,
        "chrome": chromeImport
    }    
    var choice = $('#ddlAppSelect').val()

    setTimeout(function() {
        importers[choice](valdict);
    }, 0);    
}