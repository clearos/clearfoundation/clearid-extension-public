function loadScriptGeneric(script, callback, tester=null) {
    if (tester == null || tester == "undefined") {
        var scriptGen = document.createElement('script');
        scriptGen.src = chrome.extension.getURL('js/' + script + '.js');
        scriptGen.addEventListener('load', callback, false);
        document.head.appendChild(scriptGen);
    } else {
        callback();
    }
}
function loadCryptoBackground(callback) {
    loadScriptGeneric('sodium', function() {
        loadScriptGeneric('sodium-page', callback, typeof(decryptCredential));
    }, typeof(sodium));    
}

function getBaseDomain(url) {
    var hostname = url;
    if (url.indexOf('//') > -1) {
        hostname = url.split('//')[1];
        if (hostname.indexOf('/') > -1) {
            hostname = hostname.split('/')[0];
        }
    }

    // We are interested in only the last 2 parts of the hostname (subdomains removed).
    var parts = hostname.split('.');
    return parts[parts.length-2] + '.' + parts[parts.length-1];
}

// Event when page loads in browser 
chrome.tabs.onUpdated.addListener( function (tabId, changeInfo, tab) {
    var url = tab.url;
    var urlObj = new URL(url);
    var hostname = urlObj.hostname;
    
    if (changeInfo.status == 'complete') {

        // show counter how many logins are saved for current host
        var count = get_count_local_credentials(hostname);
        set_badge_icon(count);

        
        // Check is save pending in cache && ClearID is authorized

        // Make sure ClearID is authorized to use
        if(localStorage.getItem('did')) {
            var pending = get_pending_save_by_host(hostname);
            
            if (pending) {
                send_message_to_page({
                    from: 'background',
                    subject: 'showSaveBar'
                });
            }
        }

    }
});

// Event Lister when Tab is changes
chrome.tabs.onActivated.addListener(function(tabId, changeInfo, tab) {
    chrome.tabs.query({
        active: true,
        currentWindow: true
    
    }, function(tabs) {
        
        var tab = tabs[0];
        var url = tab.url;
        if (url != "") {
            var urlObj = new URL(url);
            var hostname = urlObj.hostname;
            var count = get_count_local_credentials(hostname);
            set_badge_icon(count);
        }
    });
});

chrome.runtime.onMessage.addListener((msg, sender, sendResponse) => {
    console.log("Received background message", msg);
    // First, validate the message's structure.
    if ((msg.from === 'content') && (msg.subject === 'showPageAction')) {
        //window.open("popup.html", "extension_popup", "width=300,height=auto,status=no,scrollbars=yes,resizable=no");

        // Enable the page-action for the requesting tab.
        //chrome.pageAction.hide(sender.tab.id);
    } else if ((msg.from === 'content') && (msg.subject === 'showQRCode')) {
        window.open("qr_code.html?host=" + msg.host, "extension_popup", "width=300,height=600,status=no,scrollbars=yes,resizable=no");

    } else if ((msg.from === 'content') && (msg.subject === 'formSubmitClicked')) {

        // Make sure ClearID is authorized to use
        if(!localStorage.getItem('did')) {
            msg.error = "Not Logged In";
        }

        var pendingSave = {};
        if(localStorage.getItem('pendingSave')) {
            pendingSave = JSON.parse(localStorage.getItem('pendingSave'));
        }
        
        var creds = {};
        creds['user'] = msg.user;
        creds['password'] = msg.pass;
        creds['host'] = msg.host;
        pendingSave[msg.host] = creds;

        localStorage.setItem('pendingSave', JSON.stringify(pendingSave));

    } else if ((msg.from == 'page') && (msg.subject == 'bgClose')) {

        chrome.tabs.query({ active: true, currentWindow: true }, function(tabs) {

            var tab = tabs[0];
            var url = tab.url;
            var urlObj = new URL(url);
            var hostname = urlObj.hostname;

            // delete the domain from pending save list
            delete_host_from_save_list(hostname);

            // call the page script to hide the bar
            send_message_to_page({
                from: 'background',
                subject: 'closeSaveBar'
            });
        });

    } else if ((msg.from == 'page') && (msg.subject == 'bgNeverSave')) {

        chrome.tabs.query({ active: true, currentWindow: true }, function(tabs) {

            var tab = tabs[0];
            var url = tab.url;
            var urlObj = new URL(url);
            var hostname = urlObj.hostname;

            // delete the domain from pending save list
            delete_host_from_save_list(hostname);

            // call the page script to hide the bar
            send_message_to_page({
                from: 'background',
                subject: 'closeSaveBar'
            });
            alert("Working on it...");
        });

    } else if ((msg.from == 'page') && (msg.subject == 'bgAddSave')) {

        chrome.tabs.query({ active: true, currentWindow: true }, function(tabs) {
        
            var tab = tabs[0];
            var url = tab.url;
            var urlObj = new URL(url);
            var hostname = urlObj.hostname;

            var pendingSave = JSON.parse(localStorage.getItem('pendingSave'));

            var creds = pendingSave[hostname];
            
            // delete the domain from pending save list
            delete_host_from_save_list(hostname);

            // save the data on server
            add_credential("Test", creds.user, creds.password, hostname);
            
            // call the page script to hide the bar
            send_message_to_page({
                from: 'background',
                subject: 'closeSaveBar'
            });
        });
    } else if (sender.tab) {
        if (msg.from == "crypto" && msg.msg == "load") {
            chrome.tabs.executeScript(sender.tab.id, {file: 'js/sodium.js'}, function() {
                chrome.tabs.executeScript(sender.tab.id, {file: 'js/sodium-page.js'}, function() {
                    if (!msg.haveKeys) {
                        loadCryptoBackground(function() {
                            let kp = getCurrentAppKey(msg.hostname);
                            let robj = {from: "crypto", subject: "derived", keys: kp, success: true};

                            console.log("Delivering derived keys ", robj);
                            chrome.tabs.sendMessage(sender.tab.id, robj, function(response) {
                                if (!response.success) {
                                    console.log("Error delivering derived keys to", baseUrl);
                                }
                            });
                        });
                    }
                });
            });
        } else if (msg.from == "navigator" && msg.subject == "getStatus") {
            setTimeout(function() {
                const baseUrl = getBaseDomain(sender.tab.url);
                let storeKey = "clearid/" + baseUrl +  "/status";
                status = localStorage.getItem(storeKey);
                if (status == null || status == "null") {
                    // Perhaps the whole domain changed drastically (rebranding). Look to see if
                    // we registered a redirect for the given tab.
                    let redirect = findRedirect(sender.tab.id);
                    console.log("Detected redirect", redirect);
                    if (redirect.start != null && redirect.end == baseUrl) {
                        console.log("Checking tab target at ", redirect.start);
                        let redirectKey = "clearid/" + redirect.start +  "/status";
                        status = localStorage.getItem(redirectKey);

                        // We only need this once to help with the redirect.
                        const tabKey = "clearid/" + sender.tab.id + "/target";
                        localStorage.removeItem(tabKey);
                        localStorage.removeItem(redirectKey);

                        // Save this so that we don't lose status if the page gets refreshed.
                        localStorage.setItem(storeKey, status);
                    }
                }
                console.log("Retrieved status at " + storeKey, status);            
                sendResponse({from: "navigator", success: true, status: status});
            }, 50);
            return true;
        } else if (msg.from == "navigator" && msg.subject == "setStatus") {
            const storeKey = "clearid/" + getBaseDomain(sender.tab.url) +  "/status";
            console.log("Storing status at " + storeKey, msg.status);
            localStorage.setItem(storeKey, JSON.stringify(msg.status));
            sendResponse({from: "navigator", success: true});
        } else if (msg.from == "navigator" && msg.subject == "completed") {
            const storeKey = "clearid/" + getBaseDomain(sender.tab.url) +  "/status";
            localStorage.removeItem(storeKey);
        } else if (msg.from == "navigator" && msg.subject == "login") {
            loadCryptoBackground(function() {
                loadScriptGeneric('script', function() {
                    let requester = getBaseDomain(sender.tab.url);
                    get_password(msg.url, msg.key, requester);
                }, typeof(get_password));    
            }, typeof(decryptCredential));
        } else if (msg.from == "navigator" && msg.subject == "redirect") {
            chrome.tabs.executeScript(sender.tab.id, {file: 'js/jquery.js'}, function() {
                chrome.tabs.executeScript(sender.tab.id, {file: 'js/pwchange.js'}, function() {
                    // Send a message to indicate that the script has been executed successfully.
                    console.log("Injected scripts, sending login message to", sender.tab.url);
                    sendResponse({success: true, key: msg.key, url: msg.url});
                });
            });
            return true;
        }
    }

    sendResponse(msg, sender);
});

function findRedirect(tabId) {
    let tabKey = "clearid/" + tabId + "/target";
    var urls = localStorage.getItem(tabKey);
    if (urls != null) {
        urls = JSON.parse(urls);
    }

    var redirects = [];
    var start = null;
    var end = null;
    if (urls != null) {
        for (var i = 0; i < urls.length; i++) {
            let uset = urls[i];
            // Get the last redirect that we pushed to the list. We will make sure we have the completed
            // chain of redirects.
            let rset = null;
            if (redirects.length > 0) {
                rset = redirects[redirects.length-1];
            }
            if (uset.target != null && uset.target != uset.source) {
                if (rset == null) {
                    // This is the first one, store the start of the redirect chain.
                    start = uset.source;
                    end = uset.target;
                } else if (rset.target == uset.source) {
                    // Check whether we are chaining the redirects.
                    end = uset.target;
                }
                redirects.push(uset);
            }
        }
    }

    return {
        chain: redirects,
        start: start,
        end: end
    };
}

chrome.webNavigation["onBeforeNavigate"].addListener(function(data) {
    let targetUrl = getBaseDomain(data.url);
    let tabKey = "clearid/" + data.tabId + "/target";
    var urls = localStorage.getItem(tabKey);
    if (urls != null) {
        urls = JSON.parse(urls);
    } else {
        urls = [];
    }

    // For backward compatibility, if this is a string, overwrite it.
    if (typeof(urls) == "string") {
        urls = [{source: targetUrl, target: null}]
    } else {
        urls.push({source: targetUrl, target: null})
    }
    localStorage.setItem(tabKey, JSON.stringify(urls));
    console.log("Handling redirect with tab", data.tabId, urls);
});
chrome.webNavigation["onCommitted"].addListener(function(data) {
    if (data.transitionType == "link") {
        let tabKey = "clearid/" + data.tabId + "/target";
        var urls = localStorage.getItem(tabKey);
        if (urls != null) {
            urls = JSON.parse(urls);
        } 

        var last = urls.length-1;
        if (urls[last].target == null) {
            let targetUrl = getBaseDomain(data.url);
            urls[last].target = targetUrl;
            localStorage.setItem(tabKey, JSON.stringify(urls));
        }
        console.log("Handling redirect with tab", data.tabId, urls);
    }
});

function send_message_to_page(message)
{
    chrome.tabs.query({active: true, currentWindow: true}, function(tabs){
        chrome.tabs.sendMessage(tabs[0].id, message, function(response) {

        });  
    });
}


function set_badge_icon(count =0)
{
    if (count > 0) {

        chrome.browserAction.setBadgeText({
            text: count.toString()
        });
    
    } else {

        chrome.browserAction.setBadgeText({
            text: ""
        });
    }
    
}
