function testAuthCrypt() {
    console.log("Running authCrypt and authDecrypt unit tests.");
    kp = createKeyPair("00000000000000000seed-new-public");
    kp2 = createKeyPair("00000000000000000seed-new-pubrap");
    ac = authCrypt("message", kp2.publicKey, kp.publicKey, kp.privateKey);
    dc = authDecrypt(ac, kp2.publicKey, kp2.privateKey);
    console.assert(dc.message == "message");
    console.assert(dc.sender_vk == "FQTn2kCNuvxsKDHvHMyci41nXH4Gds6NGFffQhyvDcsq");
    console.log("authCrypt and authDecrypt tests completed.");
};

function testAnonCrypt() {
    console.log("Running tests for anonCrypt and anonDecrypt.");
    kp = createKeyPair("00000000000000000seed-new-public");
    enc = anonCrypt("message", kp.publicKey);
    dec = anonDecrypt(enc, kp.publicKey, kp.privateKey);
    console.assert(sodium.to_string(dec) == "message");
    console.log("anonCrypt tests completed.")
};

function testDataCrypt() {
    console.log("Data encryption tests running now...");
    kp = createKeyPair("00000000000000000seed-new-public");
    o = {
        "fields": [{"name": "A", "value": "B"}]
    };
    oenc = encryptData(o, kp.publicKey);
    console.assert(oenc["fields"][0]["value"] != "B", oenc["fields"]);
    odec = decryptData(oenc, kp.publicKey, kp.privateKey);
    console.assert(odec["fields"][0]["value"] == "B", odec["fields"]);
    console.log("Data encryption/decryption tests completed.");
}

function testDid() {
    console.log("Testing DID creation...");
    did = createDid("00000000000000000seed-new-public");
    console.assert(did.did == "TRryMSNui5YtXxE3fFmW82", did.did);
    console.assert(did.verkey == "FQTn2kCNuvxsKDHvHMyci41nXH4Gds6NGFffQhyvDcsq", did.verkey);
    console.log("DID tests completed.")
}

function btnTest_onclick() {
    console.log("Running unit tests...");
    let ok = {
        "auth": testAuthCrypt(),
        "anon": testAnonCrypt(),
        "data": testDataCrypt(),
        "did": testDid()
    } 
    all_ok = true;
    for (i in ok) {
        all_ok = all_ok && ok[i];
    }
    alert("All unit tests completed. Check console for details.");
};