$(function (e) {
    $("#passLengthRange").on("input change", function() {
        var pass_length = $(this).val();
        $("#pass_length").val(pass_length);
        generate_password();
    });
    $("#pass_length").on("input change", function() {
        var pass_length = $(this).val();
        $("#passLengthRange").val(pass_length);
        generate_password();
    });
    $("#include_capital_words").on("change", function() {
        generate_password();
    });
    $("#include_small_words").on("change", function() {
        generate_password();
    });
    $("#include_numbers").on("change", function() {
        generate_password();
    });
    $("#include_special_chars").on("change", function() {
        generate_password();
    });
    $("#regenerate_btn").on("click", function() {
        generate_password();
    });
    $("#copy_btn").on("click", function() {
        copy_to_clipboard();
    });
    $("#copy_btn").on("mouseout", function() {
        $("#myTooltip").text("Copy to clipboard");
    });
});

function generate_password()
{
    var pass_length = $("#pass_length").val();
    var include_capital_words = $("#include_capital_words:checked").val();
    var include_small_words = $("#include_small_words:checked").val();
    var include_numbers = $("#include_numbers:checked").val();
    var include_special_chars = $("#include_special_chars:checked").val();

    var string = make_string(pass_length, include_capital_words, include_small_words, include_numbers, include_special_chars);

    $(".random_password_string").text(string);
}

function make_string(length=5, include_capital_words =true, include_small_words =false, include_numbers =false, include_special_chars =false) 
{
    var result = "";
    var characters = "";
    
    if (include_capital_words)
        characters = characters + "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    if (include_small_words)
        characters = characters + "abcdefghijklmnopqrstuvwxyz";

    if (include_numbers)
        characters = characters + "0123456789";

    if (include_special_chars)
        characters = characters + "!@#$%^&*";

    var charactersLength = characters.length;
    
    for ( var i = 0; i < length; i++ ) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }

    return result;
}

function copy_to_clipboard(element) 
{
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val($(".random_password_string").text()).select();
    document.execCommand("copy");
    $("#myTooltip").text("Copied!");
    $temp.remove();
}

generate_password();