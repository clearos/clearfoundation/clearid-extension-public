////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

// This script is for background service. 
// In future we will use it as common on all pages

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

var api_url = "https://decentralized.clearfoundation.com/fido/";
 
function add_credential(name, user, password, url, callback)
{
    $("#form_add").text("wait...");
    var fields = make_fields(user, password);
    var fieldSets = make_field_sets(fields);

    console.log(fieldSets);

    var data = {
            'fieldSets': fieldSets,
            'token': localStorage.getItem('token'), 
            'did': localStorage.getItem('did'),
            'category': "credential",
            'url': url
        }
    data = JSON.stringify(data);
    $.ajax({
        url: api_url + 'passwordless/store',
        data: JSON.stringify(data),
        type: 'post',
        dataType: 'json',
        contentType: 'application/json',
        async: true,
        crossDomain: 'true',

        success: function(data, status) {
            console.log('stored success');
            
            if (callback) {
                fetch_vault_keys(callback);
            } else {
                fetch_vault_keys();
            }
        },
        error: function(xhr) {
            
        }
    });
}
function make_field_sets(fields, hint = "", global =false)
{
    fieldSets = [];

    fieldSet = {};

    fieldSet['fields'] = fields;
    fieldSet['hint'] = hint;
    fieldSet['global'] = global;

    fieldSets.push(fieldSet);
    return fieldSets;
}
function make_fields(user, password)
{
    fields = [];

    if (user) {
        userF = {};
        userF['name'] = 'user';
        userF['value'] = user;
        userF['global'] = false;
        fields.push(userF);
    }

    if (password) {
        passF = {};
        passF['name'] = 'password';
        passF['value'] = password;
        passF['global'] = false;
        fields.push(passF);
    }
    return fields;
}
function make_fields_card(name, number, brand, expirationMonth, expirationYear, cvv)
{
    fields = [];

    if (name) {
        userF = {};
        userF['name'] = 'name';
        userF['value'] = name;
        userF['global'] = false;
        fields.push(userF);
    }

    if (number) {
        passF = {};
        passF['name'] = 'number';
        passF['value'] = number;
        passF['global'] = false;
        fields.push(passF);
    }

    if (expirationMonth) {
        passF = {};
        passF['name'] = 'expirationMonth';
        passF['value'] = expirationMonth;
        passF['global'] = false;
        fields.push(passF);
    }

    if (expirationYear) {
        passF = {};
        passF['name'] = 'expirationYear';
        passF['value'] = expirationYear;
        passF['global'] = false;
        fields.push(passF);
    }

    if (cvv) {
        passF = {};
        passF['name'] = 'cvv';
        passF['value'] = cvv;
        passF['global'] = false;
        fields.push(passF);
    }
    return fields;
}
function make_fields_address(addressLocality, addressRegion, postalCode, streetAddress, postOfficeBoxNumber, addressCountry)
{
    fields = [];

    if (addressLocality) {
        userF = {};
        userF['name'] = 'addressLocality';
        userF['value'] = addressLocality;
        userF['global'] = false;
        fields.push(userF);
    }

    if (addressRegion) {
        passF = {};
        passF['name'] = 'addressRegion';
        passF['value'] = addressRegion;
        passF['global'] = false;
        fields.push(passF);
    }

    if (postalCode) {
        passF = {};
        passF['name'] = 'postalCode';
        passF['value'] = postalCode;
        passF['global'] = false;
        fields.push(passF);
    }

    if (streetAddress) {
        passF = {};
        passF['name'] = 'streetAddress';
        passF['value'] = streetAddress;
        passF['global'] = false;
        fields.push(passF);
    }

    if (postOfficeBoxNumber) {
        passF = {};
        passF['name'] = 'postOfficeBoxNumber';
        passF['value'] = postOfficeBoxNumber;
        passF['global'] = false;
        fields.push(passF);
    }

    if (addressCountry) {
        passF = {};
        passF['name'] = 'addressCountry';
        passF['value'] = addressCountry;
        passF['global'] = false;
        fields.push(passF);
    }
    return fields;
}
function make_fields_note(title, note, version, created)
{
    fields = [];

    if (title) {
        userF = {};
        userF['name'] = 'title';
        userF['value'] = title;
        userF['global'] = false;
        fields.push(userF);
    }

    if (note) {
        passF = {};
        passF['name'] = 'note';
        passF['value'] = note;
        passF['global'] = false;
        fields.push(passF);
    }

    if (version) {
        passF = {};
        passF['name'] = 'version';
        passF['value'] = version;
        passF['global'] = false;
        fields.push(passF);
    }

    if (created) {
        passF = {};
        passF['name'] = 'created';
        passF['value'] = created;
        passF['global'] = false;
        fields.push(passF);
    }

    return fields;
}

function fetch_vault_keys(callback =false)
{   
    $.ajax({
        url: api_url + 'passwordless/vault-keys',
        data: {
            token: localStorage.getItem('token'), 
            did: localStorage.getItem('did'), 
        },
        type: 'get',
        dataType: 'json',
        async: false,
        crossDomain: 'true',

        success: function(data, status) {
            var credentials = data.credential;
            var creditCards = data.creditCard;
            var addresses = data.address;
            var notes = data.secureNote;
            localStorage.setItem('credentials', JSON.stringify(credentials));
            localStorage.setItem('creditCards', JSON.stringify(creditCards));
            localStorage.setItem('addresses', JSON.stringify(addresses));
            localStorage.setItem('notes', JSON.stringify(notes));

            if (callback) {
                window[callback](data);
            }
        },
        error: function(xhr) {
 
        }
    });
}

function get_count_local_credentials(hostname)
{
    if (!hostname) {
        return false;
    }
    hostname = remove_sub_domain(hostname);


    var domains = JSON.parse(localStorage.getItem('credentials'));
    var credentials_length = 0;

    $.each(domains, function(key, domain) {
        
        if ((hostname) == remove_sub_domain(domain.url)) {

            $.each(domain.entries, function(k, entry) {
                credentials_length = credentials_length + 1;
            });
        }
    });

    return credentials_length;
}
function get_pending_save_by_host(hostname)
{
    pendingSave = get_pending_saved_list();
    return pendingSave[hostname];
}
var remove_sub_domain=function(v)
{
    var is_co=v.match(/\.co\./)
    v=v.split('.')
    v=v.slice(is_co ? -3: -2)
    v=v.join('.')
    return v;
}
function get_pending_saved_list()
{
    return JSON.parse(localStorage.getItem('pendingSave'));
}
function delete_host_from_save_list(hostname)
{
    var pendingSave = get_pending_saved_list();
    delete pendingSave[hostname];

    localStorage.setItem('pendingSave', JSON.stringify(pendingSave));
    return true;
}
function add_to_ignore_list(hostname)
{
    
}